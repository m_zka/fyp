
package com.example.abdullah.realprolink.Model.BidedPropertyResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BidedPropertiesResponse {

    @SerializedName("data")
    @Expose
    private List<BidedPropertites> data = null;

    public List<BidedPropertites> getData() {
        return data;
    }

    public void setData(List<BidedPropertites> data) {
        this.data = data;
    }

}
