package com.example.abdullah.realprolink.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.valueOf;


/**
 * A simple {@link Fragment} subclass.
 */
public class MortgageFragment extends Fragment {

    private TextInputLayout housepricemortgagecalc;
    private TextInputLayout yearlyInterestRate;
    private TextInputLayout yearlyTerm;
    private EditText houseprice_et_mortgagecalc;
    private EditText etYearlyInterestRate;
    private EditText etYearlyTerm;
    private TextView progressforyearly;
    private TextView mortgageAmountPrice;
    private  TextView progressforterm;
    private Button calculateMonthlyPayment;
    Pattern mPattern;

    public MortgageFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity)getActivity()).setActionBarTitle("Mortgage Property");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mortgage,container,false);



        housepricemortgagecalc = view.findViewById(R.id.input_edit_TextHousePriceMortage);
        yearlyInterestRate = view.findViewById(R.id.input_edit_TextYearlyInterestRate);
        yearlyTerm = view.findViewById(R.id.input_edit_TextYearlyTerm);
        etYearlyInterestRate = view.findViewById(R.id.editTextYearlyInterestRate);
        etYearlyTerm = view.findViewById(R.id.editTextyearlyTerm);
        mortgageAmountPrice = view.findViewById(R.id.mortgageAmount);
        houseprice_et_mortgagecalc = view.findViewById(R.id.editTextHousePricemortgagecalc);



//        yearlyInterestRate = view.findViewById(R.id.yearlyInterestRateSeekBar);
//        yearlyTerm = view.findViewById(R.id.yearlyTermSeekBar);
//        progressforterm = view.findViewById(R.id.progressYearlyTerm);

//        yearlyInterestRate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//
//
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//                double value = ((double) progress / 10.0);
//                progressforyearly.setText(valueOf(value));
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//            }
//        });


//        yearlyTerm.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//
//
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                progressforterm.setText(valueOf(progress));
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//                //write custom code to on start progress
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//
//            }
//        });


        calculateMonthlyPayment = view.findViewById(R.id.calculatePayment);

        calculateMonthlyPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{

                    submitMortgageCalcForm();

                    int houseval = Integer.parseInt(houseprice_et_mortgagecalc.getText().toString());
                    double yearlyInterestmc = Double.parseDouble(yearlyInterestRate.getEditText().toString());
                    int yearlyTermmc = (int) Integer.parseInt(yearlyTerm.getEditText().toString());


                    // Monthly intertest rate
                    yearlyInterestmc = yearlyInterestmc/100/12;
//
//
//                    // Term in months
                    yearlyTermmc = yearlyTermmc * 12;
//
                    double payment = (houseval * yearlyInterestmc) / (1 - Math.pow(1 + yearlyInterestmc, -yearlyTermmc));
//
//                    // round to two decimals
                    payment = (double)Math.round(payment * 100) / 100;
//
                    mortgageAmountPrice.setText("" + Double.toString(payment));
//
//
                }catch (NumberFormatException nfe){
                    nfe.printStackTrace();
                }


            }
        });



        return view;
    }

    private void submitMortgageCalcForm() {
        if (!validateHousePrice()) {
            return;
        }else if(!validateInterestRate()){
            return;
        }else if(!validateyearlyTerm()){
            return;
        }



//        Toasty.success(getActivity(), "Success!", Toast.LENGTH_SHORT, true).show();
//        new GlideToast.makeToast(getActivity(),"Successfully Added ",GlideToast.LENGTHLONG,GlideToast.SUCCESSTOAST,GlideToast.BOTTOM,R.drawable.ic_skyline,"#008000").show();

    }


    private boolean validateHousePrice() {
        if (houseprice_et_mortgagecalc.getText().toString().trim().isEmpty()) {
            housepricemortgagecalc.setError(getString(R.string.error_message_HousePrice));
            requestFocus(houseprice_et_mortgagecalc);
            return false;
        } else {
            housepricemortgagecalc.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateInterestRate() {
        if (etYearlyInterestRate.getText().toString().trim().isEmpty()) {
            yearlyInterestRate.setError("Yearly Interest Empty !");
            requestFocus(etYearlyInterestRate);
            return false;
        } else {
            yearlyInterestRate.setErrorEnabled(false);
        }

        return true;
    }



    private boolean validateyearlyTerm() {
        if (etYearlyTerm.getText().toString().trim().isEmpty()) {
            yearlyTerm.setError(getString(R.string.error_message_HousePrice));
            requestFocus(etYearlyTerm);
            return false;
        } else {
            yearlyTerm.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity) getContext()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



}
