package com.example.abdullah.realprolink.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.abdullah.realprolink.Model.ForgetResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import io.paperdb.Paper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PassRecoveryFragment extends Fragment {

    EditText passRecovery;
    EditText newPassword;
    Button   recovery_button,reset_button;
    String recover;
    LinearLayout linearLayout;
    LinearLayout linearLayout1;
    public PassRecoveryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view= inflater.inflate(R.layout.fragment_pass_recovery,container,false);
      //declare
        passRecovery = view.findViewById(R.id.recoveryCodeet);
        newPassword = view.findViewById(R.id.newPasswordet);
        recovery_button = view.findViewById(R.id.forget_recovery);
        reset_button=view.findViewById(R.id.btn_Recovery);


        linearLayout=view.findViewById(R.id.newpassword1);
//        linearLayout=view.findViewById(R.id.newpassword1);
//linearLayout=view.findViewById(R.id.newpassword1);
//linearLayout.setVisibility(View.VISIBLE);



        //Recovery Key
        recovery_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(passRecovery.getText())){

                    Toast.makeText(getActivity(), "Recovery Key Required is required", Toast.LENGTH_SHORT).show();
//                    progressBar.setVisibility(View.INVISIBLE);
                }

                else {


                Api api1 =RetrofitClient.getApiClient().create(Api.class);
                    Call<ForgetResponse> call1 = api1.forget(Paper.book().read("forget_email").toString(), Paper.book().read("forget_email").toString(),newPassword.getText().toString());



//                    recover();
                    Api api = RetrofitClient.getApiClient().create(Api.class);

                    Call<ResponseBody> call = api.rKey(Paper.book().read("forget_email").toString(), passRecovery.getText().toString());

                    call.enqueue(new Callback<ResponseBody>() {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            Log.i("Hello", "onResponse: " );
                            int code = response.code();

                            switch (code) {
                                case 200:

                                    recover=passRecovery.getText().toString();
                                    Paper.book().write("recovery_key",recover);

                                    linearLayout.setVisibility(View.VISIBLE);


                                    Toast.makeText(getActivity(), "key ok", Toast.LENGTH_LONG).show();
                                    // Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();


                                    break;

                                case 400:

//                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                                    Toast.makeText(getActivity(), "Wrong Code", Toast.LENGTH_SHORT).show();



                                    break;

                            }
//                progressBar.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.i("Hello", "onResponse: " + t.getMessage());
                        }

                    });











                }







            }
        });

//Password Reset

        reset_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(passRecovery.getText())){

                    Toast.makeText(getActivity(), "New Password is required", Toast.LENGTH_SHORT).show();
//                    progressBar.setVisibility(View.INVISIBLE);
                }

                else {


                    Api api1 =RetrofitClient.getApiClient().create(Api.class);
                    Call<ForgetResponse> call1 = api1.forget(Paper.book().read("forget_email").toString(), Paper.book().read("forget_key").toString(),newPassword.getText().toString());



//                    recover();
                    call1.enqueue(new Callback<ForgetResponse>() {
                        @Override
                        public void onResponse(Call<ForgetResponse> call, Response<ForgetResponse> response) {
                            int code = response.code();

                            switch (code) {
                                case 200:
                                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                                    FragmentManager fragmentManager = getFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    // Check to see if the fragment is already showing.
                                    LoginFragment loginFragment=new LoginFragment();

                                    fragmentTransaction.replace(R.id.fragment_container,loginFragment);

                                    fragmentTransaction.commit();
//                         forget_email=response.body().getUserId();
                                    //email1=response.body().getRecoveryKey();






                                    break;

                                case 400:
                                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                                    break;
                            }
//            progressBar1.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onFailure(Call<ForgetResponse> call, Throwable t) {
                            Log.i("Hello", "onResponse: " + t.getMessage());
                        }
                    });











                }







            }
        });



      return view;
    }

    private void recover() {

        Api api = RetrofitClient.getApiClient().create(Api.class);

        Call<ResponseBody> call = api.rKey(Paper.book().read("Email").toString(), passRecovery.getText().toString());

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.i("Hello", "onResponse: " );
                int code = response.code();

                switch (code) {
                    case 200:

                        recover=passRecovery.getText().toString();
                        Paper.book().write("recovery_key",recover);


                        Toast.makeText(getActivity(), "key ok", Toast.LENGTH_LONG).show();
                        // Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();


                        break;

                    case 400:

//                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                        Toast.makeText(getActivity(), "Wrong Code", Toast.LENGTH_SHORT).show();



                        break;

                }
//                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
            }

        });

    }

}
