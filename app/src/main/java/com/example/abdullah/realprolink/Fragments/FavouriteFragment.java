package com.example.abdullah.realprolink.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Adapters.UserFavouritesAdapter;
import com.example.abdullah.realprolink.Adapters.UserPropertyAdapter;
import com.example.abdullah.realprolink.Fragments.working.emptyfavouriteFragment;
import com.example.abdullah.realprolink.Model.MaxBidResponse;
import com.example.abdullah.realprolink.Model.UserInfoResponse;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserProperty;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserPropertyResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavouriteFragment extends Fragment {
    List<UserProperty> propertyResponse;
    Button btnDelete;
    String j;

    ImageButton img;
    String maxBid=null;


    private RecyclerView recyclerView;
    UserFavouritesAdapter mAdapter;

    public FavouriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity)getActivity()).setActionBarTitle("Favourite Property");
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_favourite, container, false);



        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_favourites);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        propertyResponse =new ArrayList<UserProperty>();



        ///////////////////////////////////////////////////////////

        final Api api = RetrofitClient.getApiClient().create(Api.class);

        Call<UserPropertyResponse> call = api.favourite( Paper.book().read("user_id").toString());

        call.enqueue(new Callback<UserPropertyResponse>() {
            @Override
            public void onResponse(Call<UserPropertyResponse> call, final Response<UserPropertyResponse> response) {
                int code = response.code();

                switch (code) {
                    case 200:
                        Paper.book().write("lsize",1);
                        //  Toast.makeText(AddProperty.this, "Success", Toast.LENGTH_SHORT).show();
                        propertyResponse = response.body().getData();





                        //propertyResponse.get(1).getPId();

                        //get property ID from Response

                        mAdapter = new UserFavouritesAdapter(getContext(),propertyResponse);

                        // UserPropertyAdapter adapter = new UserPropertyAdapter(getContext(), propertyResponse);
                        recyclerView.setAdapter(mAdapter);

                        mAdapter.setOnItemClickListener(new UserFavouritesAdapter.onItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                int i=position;
                                propertyResponse.get(i).getPId();
                                j =  propertyResponse.get(i).getPId();

                                Toast.makeText(getContext(), response.body().getData().get(0).getUrl(), Toast.LENGTH_SHORT).show();
                                // Values set for Property Detail Fragment

                                Paper.book().write("d_price",propertyResponse.get(i).getPPrice());
                                Paper.book().write("d_title",propertyResponse.get(i).getPTitle());
                                Paper.book().write("d_address",propertyResponse.get(i).getPLocation().concat(propertyResponse.get(i).getpCity()));
                                Paper.book().write("d_beds",propertyResponse.get(i).getPBeds());
                                Paper.book().write("d_baths",propertyResponse.get(i).getPBaths());
                                Paper.book().write("d_pid",propertyResponse.get(i).getPId());
                                Paper.book().write("d_purpose",propertyResponse.get(i).getpPurpose());
                                Paper.book().write("d_type",propertyResponse.get(i).getpType());
                                Paper.book().write("d_user_id",propertyResponse.get(i).getpUserID());

                                Paper.book().write("d_area",propertyResponse.get(i).getPArea());
                                Paper.book().write("d_desc",propertyResponse.get(i).getPDesc());




                                ////////////////////////////////////////////////////////////////////////////////



                                Call<MaxBidResponse> call = api.maxBid(propertyResponse.get(i).getPId());

                                call.enqueue(new Callback<MaxBidResponse>() {
                                    @Override
                                    public void onResponse(Call<MaxBidResponse> call, Response<MaxBidResponse> response) {


                                        if(response.body().getBidAmount()==null){





                                            Paper.book().write("maxbid","000");

                                        }
                                        else{
                                            maxBid=response.body().getBidAmount();
                                            Paper.book().write("maxbid",response.body().getBidAmount());
                                            Toast.makeText(getActivity(), Paper.book().read("maxbid").toString(), Toast.LENGTH_SHORT).show();

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<MaxBidResponse> call, Throwable t) {

                                    }
                                });



                                /////////////////////////////////////////////////////////////////////////////////







                                Call<UserInfoResponse> call2 = api.ReadUser(
                                        Paper.book().read("d_user_id").toString()



                                );

                                call2.enqueue(new Callback<UserInfoResponse>() {
                                    @Override
                                    public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                                        int code = response.code();

                                        switch (code) {
                                            case 200:



                                                Paper.book().write("agent_first_name",response.body().getFirstName());
                                                Paper.book().write("agent_last_name",response.body().getLastName());
                                                Paper.book().write("agent_phone",response.body().getUserMobile());
                                                Paper.book().write("agent_url",response.body().getUrl());
                                                  Toast.makeText(getActivity(), Paper.book().read("agent_first_name").toString(), Toast.LENGTH_SHORT).show();




                                                break;

//



                                            case 400:
                                                break;
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<UserInfoResponse> call, Throwable t) {

                                    }
                                });



                                /////////////////////////////////////////////////////////////////////////////////
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.fMain,new AllPropertiesFragement());
                                ft.addToBackStack(null);
                                ft.commit();
                            }

                            @Override
                            public void onDeleteClick(int position) {
                                int i=position;

                                deleteFav(i);



                            }
                        });
//                        recyclerView.addOnItemTouchListener(
//
//                                new RecyclerItemClickListener(context, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
//                                    @Override public void onItemClick(View view, int position) {
//                                        int i = position;
//                                        j =  propertyResponse.get(i).getPId();
//
//                                        Toast.makeText(getActivity(), j, Toast.LENGTH_SHORT).show();
//                                      //  Toast.makeText(getActivity(), Paper.book().read("p_idd").toString(), Toast.LENGTH_SHORT).show();
//
//                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
//                                        ft.replace(R.id.fMain,new CompareFragment());
//                                        ft.addToBackStack(null);
//                                        ft.commit();
//
//
//
//
//
//                                    }
//
//                                    @Override public void onLongItemClick(View view, int position) {
//                                        // do whatever
//                                    }
//                                })
//                        );
                        break;

                    case 400:
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.fMain,new emptyfavouriteFragment());

                        ft.addToBackStack(null);
                        ft.commit();

                        break;
                }
//            progressBar1.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UserPropertyResponse> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
            }
        });





        return  view;


    }

    private void deleteFav(int i) {

        String p_id=  propertyResponse.get(i).getPId();

        Toast.makeText(getActivity(), p_id, Toast.LENGTH_SHORT).show();

        Api api = RetrofitClient.getApiClient().create(Api.class);
        Call<ResponseBody>call=  api.delFavourites(Paper.book().read("user_id").toString(),p_id);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                int code = response.code();

                switch (code) {
                    case 200:
                        Toast.makeText(getActivity(), "Favourite Deleted", Toast.LENGTH_SHORT).show();
                        break;

                    case 400:
                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        propertyResponse.remove(i);
        mAdapter.notifyDataSetChanged();
    }

    }


