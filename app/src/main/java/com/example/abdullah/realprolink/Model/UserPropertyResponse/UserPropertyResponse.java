
package com.example.abdullah.realprolink.Model.UserPropertyResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserPropertyResponse {

    @SerializedName("data")
    @Expose
    private List<UserProperty> data = null;

    public List<UserProperty> getData() {
        return data;
    }

    public void setData(List<UserProperty> data) {
        this.data = data;
    }

}
