package com.example.abdullah.realprolink.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.abdullah.realprolink.Activity.NoConnectionActivity;
import com.example.abdullah.realprolink.Activity.ProfileActivity;
import com.example.abdullah.realprolink.Activity.SplashActivity;
import com.example.abdullah.realprolink.Model.UserInfoResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static android.support.v4.content.ContextCompat.getSystemService;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment {

    TextView name,email,phone,address,city;
    CircularImageView imageView;




    public static UserProfileFragment newInstance() {
        return new UserProfileFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile,container,false);
        Paper.init(getContext());
        imageView =view.findViewById(R.id.user_img);
        final String BaseURL="http://192.168.1.100:8080/FYP/api/uploads/";
final Context context;




        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Profile");
        setHasOptionsMenu(true);

        name=view.findViewById(R.id.txt_userProfile_name);
        email=view.findViewById(R.id.txt_userProfile_email);
        phone=view.findViewById(R.id.txt_userProfile_phone);
        address=view.findViewById(R.id.txt_userProfile_address);
        city=view.findViewById(R.id.txt_userProfile_city);

        Api api = RetrofitClient.getApiClient().create(Api.class);

        Call<UserInfoResponse> call = api.ReadUser(
                Paper.book().read("user_id").toString()



        );

        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                int code = response.code();

                switch (code) {
                    case 200:

                       // Paper.book().write("fname",response.body().getFirstName());
                      //  Toast.makeText(getActivity(), response.body().getFirstName(), Toast.LENGTH_SHORT).show();

                        address.setText(response.body().getUserAddress());
                        city.setText(response.body().getUserCity());
                      email.setText(response.body().getUserEmail());
                        name.setText(response.body().getFirstName().concat(" ").concat(response.body().getLastName()));
                       phone.setText(response.body().getUserMobile());



                       Paper.book().write("first_name",response.body().getFirstName());
                        Paper.book().write("last_name",response.body().getLastName());

                        Paper.book().write("city",response.body().getUserCity());
                        Paper.book().write("address",response.body().getUserAddress());

                        Paper.book().write("phone",response.body().getUserMobile());
                        Paper.book().write("p_url",response.body().getUrl());


                          Toast.makeText(getContext(), Paper.book().read("p_url").toString(), Toast.LENGTH_SHORT).show();

                       Glide.with(getActivity().getApplicationContext()).load(BaseURL+Paper.book().read("p_url"))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)

                                .into(imageView);
                       // Picasso.get().load(BaseURL.concat(Paper.book().read("p_url").toString())).into(imageView);
//                        FragmentManager fragmentManager =getFragmentManager();
//                        FragmentTransaction fragmentTransaction =fragmentManager.beginTransaction();
//                        fragmentTransaction.replace(R.id.fragment_userProfile,new UserProfileFragment());
//                        fragmentTransaction.addToBackStack(null);
//                        fragmentTransaction.commit();


                        break;

//



                    case 400:
                        //    Toast.makeText(AddProperty.this, "Failed", Toast.LENGTH_SHORT).show();
                        break;
                }
//            progressBar1.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
                Log.i("Hello", "onResponse: " + t.getMessage());
//                // Toast.makeText(SplashActivity.this, "Call Failed", Toast.LENGTH_SHORT).show();
                final Intent intent = new Intent(getActivity(), NoConnectionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });











        return view;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.items, menu);
//        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.editProfile:
                EditProfileFragment dialog = new EditProfileFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.addToBackStack(null);
                dialog.show(ft, EditProfileFragment.TAG);
                return true;
        }

        return false;
    }


}
