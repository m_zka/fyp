
package com.example.abdullah.realprolink.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyBidResponse {

    @SerializedName("p_id")
    @Expose
    private String pId;
    @SerializedName("min_bid")
    @Expose
    private String minBid;

    public String getPId() {
        return pId;
    }

    public void setPId(String pId) {
        this.pId = pId;
    }

    public String getMinBid() {
        return minBid;
    }

    public void setMinBid(String minBid) {
        this.minBid = minBid;
    }

}
