package com.example.abdullah.realprolink.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Adapters.BidedPropertiesAdapter;
import com.example.abdullah.realprolink.Adapters.PropertiesAdapter;
import com.example.abdullah.realprolink.Fragments.AllPropertiesFragement;
import com.example.abdullah.realprolink.Fragments.working.emptyfavouriteFragment;
import com.example.abdullah.realprolink.Model.BidedPropertyResponse.BidedPropertiesResponse;
import com.example.abdullah.realprolink.Model.BidedPropertyResponse.BidedPropertites;
import com.example.abdullah.realprolink.Model.MaxBidResponse;
import com.example.abdullah.realprolink.Model.PropertyBidResponse;
import com.example.abdullah.realprolink.Model.UserInfoResponse;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserProperty;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserPropertyResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class BidedPropertiesFragement extends Fragment {

    List<BidedPropertites> bidedpropertyResponse;


    Button btnDelete;
    String j;

    ImageButton img;
    String maxBid=null;


    private RecyclerView recyclerView;
    BidedPropertiesAdapter mAdapter;
    public BidedPropertiesFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity)getActivity()).setActionBarTitle("Bided Properties");
        View view= inflater.inflate(R.layout.fragment_bided_properties_fragement, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_bidedProperties);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        bidedpropertyResponse =new ArrayList<BidedPropertites>();





        final Api api = RetrofitClient.getApiClient().create(Api.class);

        Call<BidedPropertiesResponse> call = api.getBidProp(Paper.book().read("user_id").toString() );

        call.enqueue(new Callback<BidedPropertiesResponse>() {
            @Override
            public void onResponse(final Call<BidedPropertiesResponse> call, final Response<BidedPropertiesResponse> response) {
                int code = response.code();

                switch (code) {
                    case 200:
                        Paper.book().write("lsize",1);
                        //  Toast.makeText(AddProperty.this, "Success", Toast.LENGTH_SHORT).show();
                        bidedpropertyResponse = response.body().getData();




                        //propertyResponse.get(1).getPId();

                        //get property ID from Response

                        mAdapter = new BidedPropertiesAdapter(getContext(),bidedpropertyResponse);

                        // UserPropertyAdapter adapter = new UserPropertyAdapter(getContext(), propertyResponse);
                        recyclerView.setAdapter(mAdapter);

                        mAdapter.setOnItemClickListener(new BidedPropertiesAdapter.onItemClickListener() {

                            @Override
                            public void onItemClick(int position) {
                                int i=position;
                                int c=0;
                                bidedpropertyResponse.get(i).getPId();
                                j =  bidedpropertyResponse.get(i).getPId();

//                                Toast.makeText(getActivity(), j, Toast.LENGTH_SHORT).show();
                                // Values set for Property Detail Fragment

                                Paper.book().write("d_price",bidedpropertyResponse.get(i).getPPrice());
                                Paper.book().write("d_title",bidedpropertyResponse.get(i).getPTitle());
                                Paper.book().write("d_address",bidedpropertyResponse.get(i).getPLocation().concat(bidedpropertyResponse.get(i).getPCity()));
                                Paper.book().write("d_beds",bidedpropertyResponse.get(i).getPBeds());
                                Paper.book().write("d_baths",bidedpropertyResponse.get(i).getPBaths());
                                Paper.book().write("d_pid",bidedpropertyResponse.get(i).getPId());
                                Paper.book().write("d_purpose",bidedpropertyResponse.get(i).getpPurpose());
                                Paper.book().write("d_type",bidedpropertyResponse.get(i).getpType());
                                Paper.book().write("d_user_id",bidedpropertyResponse.get(i).getpUserID());
                                Paper.book().write("d_area",bidedpropertyResponse.get(i).getPArea());
                                Paper.book().write("d_desc",bidedpropertyResponse.get(i).getPDesc());






                                Call<MaxBidResponse> call1 = api.maxBid(j);

                                call1.enqueue(new Callback<MaxBidResponse>() {
                                    @Override
                                    public void onResponse(Call<MaxBidResponse> call1, Response<MaxBidResponse> response) {


                                        if(response.body().getBidAmount()==null){


                                            Call<PropertyBidResponse> call4 = api.property_bid(j);


                                            call4.enqueue(new Callback<PropertyBidResponse>() {
                                                @Override
                                                public void onResponse(Call<PropertyBidResponse> call, Response<PropertyBidResponse> response) {
                                                    Paper.book().write("maxbid", response.body().getMinBid());

//                                                    Toast.makeText(getActivity(), "innner", Toast.LENGTH_SHORT).show();
                                                }

                                                @Override
                                                public void onFailure(Call<PropertyBidResponse> call, Throwable t) {

                                                }
                                            });



                                        }
                                        else {

                                            maxBid = response.body().getBidAmount();
                                            Paper.book().write("maxbid", response.body().getBidAmount());
//                                            Toast.makeText(getActivity(), response.body().getBidAmount(), Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<MaxBidResponse> call, Throwable t) {

                                    }
                                });





//
//

                                Call<UserInfoResponse> call2 = api.ReadUser(
                                        Paper.book().read("d_user_id").toString()



                                );

                                call2.enqueue(new Callback<UserInfoResponse>() {
                                    @Override
                                    public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                                        int code = response.code();

                                        switch (code) {
                                            case 200:



                                                Paper.book().write("agent_first_name",response.body().getFirstName());
                                                Paper.book().write("agent_last_name",response.body().getLastName());
                                                Paper.book().write("agent_phone",response.body().getUserMobile());
                                                Paper.book().write("agent_url",response.body().getUrl());
//                                                Toast.makeText(getActivity(), Paper.book().read("agent_first_name").toString(), Toast.LENGTH_SHORT).show();




                                                break;

//



                                            case 400:
                                                break;
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<UserInfoResponse> call, Throwable t) {

                                    }
                                });



                                /////////////////////////////////////////////////////////////////////////////////
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.fMain,new AllPropertiesFragement());
                                ft.addToBackStack(null);
                                ft.commit();

                            }

                            @Override
                            public void onDeleteClick(int position) {
                                int i=position;

                                DeleteBidedProperty(i);




                            }
                        });
//                        recyclerView.addOnItemTouchListener(
//
//                                new RecyclerItemClickListener(context, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
//                                    @Override public void onItemClick(View view, int position) {
//                                        int i = position;
//                                        j =  propertyResponse.get(i).getPId();
//
//                                        Toast.makeText(getActivity(), j, Toast.LENGTH_SHORT).show();
//                                      //  Toast.makeText(getActivity(), Paper.book().read("p_idd").toString(), Toast.LENGTH_SHORT).show();
//
//                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
//                                        ft.replace(R.id.fMain,new CompareFragment());
//                                        ft.addToBackStack(null);
//                                        ft.commit();
//
//
//
//
//
//                                    }
//
//                                    @Override public void onLongItemClick(View view, int position) {
//                                        // do whatever
//                                    }
//                                })
//                        );
                        break;

                    case 400:
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.fMain,new emptyfavouriteFragment());

                        ft.addToBackStack(null);
                        ft.commit();
                        Paper.book().write("lsize",0);
                        break;
                }
//            progressBar1.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<BidedPropertiesResponse> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
            }
        });








        return view;
    }

    private void DeleteBidedProperty(int i) {
        final Api api = RetrofitClient.getApiClient().create(Api.class);


        Call<ResponseBody> call = api.deleteBid(Paper.book().read("user_id").toString(),bidedpropertyResponse.get(i).getPId() );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        bidedpropertyResponse.remove(i);
        mAdapter.notifyDataSetChanged();
    }

}
