package com.example.abdullah.realprolink.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.Login;
import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Activity.MapsActivity;
import com.example.abdullah.realprolink.Activity.NoConnectionActivity;
import com.example.abdullah.realprolink.Adapters.ViewPagerAdapter;
import com.example.abdullah.realprolink.Model.MyBidResponse;
import com.example.abdullah.realprolink.Model.URLResponse.URLResponse;
import com.example.abdullah.realprolink.Model.URLResponse.URLS;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserPropertyResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import java.util.List;

import io.paperdb.Paper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Thread.sleep;


/**
 * A simple {@link Fragment} subclass.
 */
public class AllPropertiesFragement extends Fragment   {

    TextView title,price,address,currentBid,beds,baths,area,p_id,type,purpose,location,agentName,desc,phone,mybid;
ImageButton img;
    List<URLS> URLS;

    final Api api = RetrofitClient.getApiClient().create(Api.class);


    LinearLayout placeBid,maps;

    String bids;
    String Mybid;
    public AllPropertiesFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity)getActivity()).setActionBarTitle("All Properties");
        final View view =inflater.inflate(R.layout.fragment_all_properties_fragement, container, false);

//img=view.findViewById(R.id.myProperty_editProperty);
        Call<URLResponse> call =api.getUri(Paper.book().read("d_title").toString());
        call.enqueue(new Callback<URLResponse>() {
            @Override
            public void onResponse(Call<URLResponse> call, Response<URLResponse> response) {

                URLS=response.body().getData();
                int i= URLS.size();
                String[] strings=new String[i];
                for (int j=0;j<=i-1;j++){
                    strings[j]="http://192.168.1.100:8080/FYP/api/uploads/".concat(URLS.get(j).getUrl());
//            Toast.makeText(getContext(),strings[j],Toast.LENGTH_LONG).show();

                }
//      murls.add(i)="http://onlinenoticeboard.website/api/uploads/".concat(String.valueOf(urls.get(i).getUrl()));
//
//}
                TabLayout mTabLayout = view.findViewById(R.id.tab_layout_allproperties);
                ViewPager viewPager = view.findViewById(R.id.ViewPager_allproperties);

                ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getContext(), strings);
                viewPager.setAdapter(viewPagerAdapter);
                mTabLayout.setupWithViewPager(viewPager, true);

            }



            @Override
            public void onFailure(Call<URLResponse> call, Throwable t) {

            }
        });


        title=view.findViewById(R.id.AllProp_title);
        price=view.findViewById(R.id.AllProp_price);
        address=view.findViewById(R.id.AllProp_address);
        currentBid=view.findViewById(R.id.AllProp_current_bid);
        beds=view.findViewById(R.id.AllProp_beds);
        baths=view.findViewById(R.id.AllProp_baths);
        area=view.findViewById(R.id.AllProp_area);
        p_id=view.findViewById(R.id.AllProp_id);
        type=view.findViewById(R.id.AllProp_type);
        purpose=view.findViewById(R.id.AllProp_purpose);
        location=view.findViewById(R.id.AllProp_location);
        agentName=view.findViewById(R.id.AllProp_agentName);
        desc=view.findViewById(R.id.AllProp_description);
        phone=view.findViewById(R.id.AllProp_agentNumber);
        mybid=view.findViewById(R.id.AllProp_my_bid);


        maps=view.findViewById(R.id.allprop_maps);


        price.setText(Paper.book().read("d_price").toString());
        title.setText(Paper.book().read("d_title").toString());
        address.setText(Paper.book().read("d_address").toString());
        location.setText(Paper.book().read("d_address").toString());
        beds.setText(Paper.book().read("d_beds").toString());
        baths.setText(Paper.book().read("d_baths").toString());
        p_id.setText(Paper.book().read("d_pid").toString());
        desc.setText(Paper.book().read("d_desc").toString());
//
        area.setText(Paper.book().read("d_area").toString());


        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MapsActivity.class));
            }
        });


        switch (Paper.book().read("d_type").toString()){

            case "0":
                type.setText("House");
                break;
            case "1":
                type.setText("Plot");
                break;
            case "2":
                type.setText("Commercial");
                break;




        }

        switch (Paper.book().read("d_purpose").toString()){

            case "0":
                purpose.setText("Sale");
                break;
            case "1":
                purpose.setText("Rent");
                break;




        }

        placeBid = view.findViewById(R.id.AllProp_Place_bid);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                currentBid.setText(Paper.book().read("maxbid").toString());

                phone.setText(Paper.book().read("agent_phone").toString());

                agentName.setText(Paper.book().read("agent_first_name").toString().concat(" ").concat(Paper.book().read("agent_last_name").toString()));




                placeBid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Paper.book().read("user_id") != null) {
                            final EditText bid = new EditText(getContext());
                            bid.setInputType(InputType.TYPE_CLASS_NUMBER);
// Set the default text to a link of the Queen
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Add Bid");

                            View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.popup_dialog_placebid, (ViewGroup) getView(), false);

                            new AlertDialog.Builder(getContext())
                                    .setTitle("Enter Your Bid")

                                    .setView(bid)


                                    .setPositiveButton("Bid", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            bids = bid.getText().toString();
//                                            Toast.makeText(getActivity(), bids, Toast.LENGTH_SHORT).show();
                                            if (Integer.parseInt(bids) > Integer.parseInt(currentBid.getText().toString())) {

                                                Call<ResponseBody> call = api.addBid(Paper.book().read("user_id").toString(),
                                                        Paper.book().read("d_pid").toString(), bids);
                                                call.enqueue(new Callback<ResponseBody>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                        Intent intent = new Intent(getActivity(), Login.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                                                    }
                                                });


                                            }
                                            else
                                            {
//                                                Toast.makeText(getActivity(), "Your Bid Should be more than Current Bid", Toast.LENGTH_SHORT).show();

                                            }
                                        }

                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {

                                        }
                                    })
                                    .show();


                        }

                        else{
                            Intent intent = new Intent(getActivity(), Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        }


                    }

                });
                if (Paper.book().read("user_id") != null) {

                    Call<MyBidResponse> call1 = api.myBid(Paper.book().read("user_id").toString(),
                            Paper.book().read("d_pid").toString());
                    call1.enqueue(new Callback<MyBidResponse>() {
                        @Override
                        public void onResponse(Call<MyBidResponse> call, Response<MyBidResponse> response) {


                            if (response.body().getBidAmount() != null) {


                                Mybid = response.body().getBidAmount();
                                mybid.setText(Mybid);
//                                Toast.makeText(getActivity(), "Success".concat(Mybid), Toast.LENGTH_SHORT).show();
                            } else {
                                Mybid = "No Bid";
                                mybid.setText(Mybid);

                            }


                        }

                        @Override
                        public void onFailure(Call<MyBidResponse> call, Throwable t) {
//                            Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();

                        }
                    });
                }




            }
        }, 200); // Millisecond 1000 = 1 sec




//



        return  view;

    }


}
