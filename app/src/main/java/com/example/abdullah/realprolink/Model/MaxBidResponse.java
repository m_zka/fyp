
package com.example.abdullah.realprolink.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaxBidResponse {

    @SerializedName("Bid id")
    @Expose
    private String bidId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("p_id")
    @Expose
    private String pId;
    @SerializedName("bid_amount")
    @Expose
    private String bidAmount;

    public String getBidId() {
        return bidId;
    }

    public void setBidId(String bidId) {
        this.bidId = bidId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPId() {
        return pId;
    }

    public void setPId(String pId) {
        this.pId = pId;
    }

    public String getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(String bidAmount) {
        this.bidAmount = bidAmount;
    }

}
