package com.example.abdullah.realprolink.Fragments.working;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.ConnectionRequest;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Adapters.SpinnerAdapter;
import com.example.abdullah.realprolink.Adapters.UserPropertyAdapter;
import com.example.abdullah.realprolink.Fragments.FavouriteFragment;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserProperty;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserPropertyResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;
import com.example.abdullah.realprolink.SearchPropertyResultFragement;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private Button homebutton, plotbutton, commercialbutton;
    private Button salebutton, rentbutton;
    private Button searchbtn;
    LinearLayout layout_baths,layout_beds;

    String type="",purpose="";
    String beds;
    String baths,code;
    EditText city,min_area,max_area,min_price,max_price;
    Spinner spinnerbedroom,spinnerbathroom;
    MaterialSearchBar materialSearchBarcity;
    private FusedLocationProviderClient mFusedLocationProviderClientCity;
    private PlacesClient placesClientCity;
    private List<AutocompletePrediction> predictionListCity;


   public List<UserProperty> propertyResponse;






    private RecyclerView recyclerView;
    UserPropertyAdapter mAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity)getActivity()).setActionBarTitle("Search Property");
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_search, container, false);
layout_baths=view.findViewById(R.id.linear_baths);
        layout_beds=view.findViewById(R.id.linear_beds);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_Search);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        propertyResponse =new ArrayList<UserProperty>();





        materialSearchBarcity = view.findViewById(R.id.searchBarCitySearch);

        mFusedLocationProviderClientCity = LocationServices.getFusedLocationProviderClient(getActivity());
        Places.initialize(getActivity(),"AIzaSyCAw94EuY9EnZMukKaec5R5qnS_D4CFpAM");

        placesClientCity=Places.createClient(getActivity());
        final AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

        materialSearchBarcity.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                getActivity().startSearch(text.toString(), true, null, true);
            }

            @Override
            public void onButtonClicked(int buttonCode) {
                if (buttonCode == MaterialSearchBar.BUTTON_NAVIGATION) {
                    //opening or closing a navigation drawer
                } else if (buttonCode == MaterialSearchBar.BUTTON_BACK) {
                    materialSearchBarcity.disableSearch();
                }
            }
        });

        materialSearchBarcity.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                FindAutocompletePredictionsRequest predictionsRequest = FindAutocompletePredictionsRequest.builder()
                        .setTypeFilter(TypeFilter.CITIES)
                        .setCountry("pk")
                        .setSessionToken(token)
                        .setQuery(s.toString())
                        .build();
                placesClientCity.findAutocompletePredictions(predictionsRequest).addOnCompleteListener(new OnCompleteListener<FindAutocompletePredictionsResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<FindAutocompletePredictionsResponse> task) {
                        if (task.isSuccessful()) {
                            FindAutocompletePredictionsResponse predictionsResponse = task.getResult();
                            if (predictionsResponse != null) {
                                predictionListCity = predictionsResponse.getAutocompletePredictions();
                                List<String> suggestionsList = new ArrayList<>();
                                for (int i = 0; i < predictionListCity.size(); i++) {
                                    AutocompletePrediction prediction = predictionListCity.get(i);
                                    suggestionsList.add(prediction.getFullText(null).toString());
                                }
                                materialSearchBarcity.updateLastSuggestions(suggestionsList);
                                if (!materialSearchBarcity.isSuggestionsVisible()) {
                                    materialSearchBarcity.showSuggestionsList();
                                }
                            }
                        } else {
                            Log.i("mytag", "prediction fetching task unsuccessful");
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        materialSearchBarcity.setSuggstionsClickListener(new SuggestionsAdapter.OnItemViewClickListener() {
            @Override
            public void OnItemClickListener(int position, View v) {
                if (position >= predictionListCity.size()) {
                    return;
                }
                AutocompletePrediction selectedPrediction = predictionListCity.get(position);
                String suggestion = materialSearchBarcity.getLastSuggestions().get(position).toString();
                materialSearchBarcity.setText(suggestion);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        materialSearchBarcity.clearSuggestions();
                    }
                }, 1000);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(materialSearchBarcity.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
                final String placeId = selectedPrediction.getPlaceId();
                final List<Place.Field> placeFields = Arrays.asList(Place.Field.LAT_LNG);

                FetchPlaceRequest fetchPlaceRequest = FetchPlaceRequest.builder(placeId, placeFields).build();
                placesClientCity.fetchPlace(fetchPlaceRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                    @Override
                    public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                        Place place = fetchPlaceResponse.getPlace();



                        Log.i("mytag", "Place found: " + place.getName());
                        LatLng latLngOfPlace = place.getLatLng();
                        // Toast.makeText(getContext(),place.getAddress(),Toast.LENGTH_LONG).show();



                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            apiException.printStackTrace();
                            int statusCode = apiException.getStatusCode();
                            Log.i("mytag", "place not found: " + e.getMessage());
                            Log.i("mytag", "status code: " + statusCode);
                        }
                    }
                });
            }

            @Override
            public void OnItemDeleteListener(int position, View v) {

            }
        });

        min_area=view.findViewById(R.id.Searchminareaedittext);
        max_area=view.findViewById(R.id.Searchmaxareaedittext);
        min_price=view.findViewById(R.id.SearchminPrcieedittext);
        max_price=view.findViewById(R.id.SearchmaxPrcieedittext);

        materialSearchBarcity.setText("");
        min_area.setText("");
        max_area.setText("");
        min_price.setText("");
        max_price.setText("");

        homebutton = view.findViewById(R.id.SearchhomeButton);
        plotbutton = view.findViewById(R.id.SearchplotButton);
        commercialbutton = view.findViewById(R.id.SearchcommercialButton);
        rentbutton = view.findViewById(R.id.SearchrentButton);
        salebutton = view.findViewById(R.id.SearchsaleButton);

        searchbtn=view.findViewById(R.id.SearchPropertyButton);


        spinnerbedroom =view.findViewById(R.id.Search_bedroom_spinner);
         spinnerbathroom =view.findViewById(R.id.Search_bathroom_spinner);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.numbers,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerbedroom.setAdapter(adapter);
        spinnerbedroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                beds = parent.getItemAtPosition(position).toString();
                Toast.makeText(getActivity(),beds,Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        spinnerbathroom.setAdapter(adapter);
        spinnerbathroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                baths = parent.getItemAtPosition(position).toString();
                Toast.makeText(getActivity(),beds,Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });







        homebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                homebutton.setTextColor(Color.WHITE);
                type="0";
                plotbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                plotbutton.setTextColor(Color.BLUE);
                commercialbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                commercialbutton.setTextColor(Color.BLUE);
                layout_baths.setVisibility(View.VISIBLE);
                layout_beds.setVisibility(View.VISIBLE);
            }
        });

        plotbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plotbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                plotbutton.setTextColor(Color.WHITE);
                type="1";
                homebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                homebutton.setTextColor(Color.BLUE);
                commercialbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                commercialbutton.setTextColor(Color.BLUE);
                layout_baths.setVisibility(View.GONE);
                layout_beds.setVisibility(View.GONE);

            }
        });

        commercialbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commercialbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                commercialbutton.setTextColor(Color.WHITE);
                type="2";
                homebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                homebutton.setTextColor(Color.BLUE);
                plotbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                plotbutton.setTextColor(Color.BLUE);

                layout_baths.setVisibility(View.GONE);
                layout_beds.setVisibility(View.GONE);
            }
        });

        rentbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rentbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                rentbutton.setTextColor(Color.WHITE);
                purpose="1";
                salebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                salebutton.setTextColor(Color.BLUE);
            }
        });

        salebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                salebutton.setTextColor(Color.WHITE);
                purpose="0";
                rentbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                rentbutton.setTextColor(Color.BLUE);

            }
        });

        searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), beds, Toast.LENGTH_SHORT).show();
                searchProperty();

            }
        });



        return view;
    }

    private void searchProperty() {



        Api api = RetrofitClient.getApiClient().create(Api.class);
        Call<UserPropertyResponse> call=  api.search(
                beds,baths,
             min_price.getText().toString(),
                max_area.getText().toString(),
                min_area.getText().toString(),
                max_area.getText().toString(),
                materialSearchBarcity.getText(),purpose,type
                );




        call.enqueue(new Callback<UserPropertyResponse>() {
            @Override
            public void onResponse(Call<UserPropertyResponse> call, Response<UserPropertyResponse> response) {

                int code = response.code();

                switch (code) {
                    case 200:
                        Paper.book().write("lsize",1);
                        //  Toast.makeText(AddProperty.this, "Success", Toast.LENGTH_SHORT).show();
                        propertyResponse = response.body().getData();

                        // UserPropertyAdapter adapter = new UserPropertyAdapter(getContext(), propertyResponse);


               //    Toast.makeText(getActivity(), response.body().getData().get(1).getPLocation(), Toast.LENGTH_SHORT).show();
                        Paper.book().write("responses",response.body().getData());
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.fMain,new SearchPropertyResultFragement());
                        ft.addToBackStack(null);
                        ft.commit();
//
                        break;

                    case 400:
                        FragmentTransaction ft1 = getFragmentManager().beginTransaction();
                        ft1.replace(R.id.fMain,new emptyfavouriteFragment());

                        ft1.addToBackStack(null);
                        ft1.commit();
                        break;
                }
            }

            @Override
            public void onFailure(Call<UserPropertyResponse> call, Throwable t) {

            }
        });
    }

    public List<UserProperty> getRespone() {

        return propertyResponse;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public List<UserProperty> getProperty()
    {
        return propertyResponse;
    }

}
