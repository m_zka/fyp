package com.example.abdullah.realprolink.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Adapters.AgentListAdapter;
import com.example.abdullah.realprolink.Adapters.PropertiesAdapter;
import com.example.abdullah.realprolink.Model.AgentListResponse.AgentsListResponse;
import com.example.abdullah.realprolink.Model.AgentListResponse.AgentsResponse;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserProperty;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserPropertyResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class AgentsListFragement extends Fragment {

    List<AgentsResponse> agentsResponses;

    private RecyclerView recyclerView;
    AgentListAdapter mAdapter;

    public AgentsListFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity)getActivity()).setActionBarTitle("Agents List");
        View view = inflater.inflate(R.layout.fragment_agents_list_fragement, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_agentList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        agentsResponses =new ArrayList<AgentsResponse>();

        final Api api = RetrofitClient.getApiClient().create(Api.class);

        Call<AgentsListResponse> call = api.agentsList( );

        call.enqueue(new Callback<AgentsListResponse>() {
            @Override
            public void onResponse(Call<AgentsListResponse> call, Response<AgentsListResponse> response) {

                agentsResponses=response.body().getData();
               // Toast.makeText(getContext(),agentsResponses.get(0).getFirstName(),Toast.LENGTH_LONG).show();
                mAdapter = new AgentListAdapter(getContext(),agentsResponses);

                // UserPropertyAdapter adapter = new UserPropertyAdapter(getContext(), propertyResponse);
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<AgentsListResponse> call, Throwable t) {

            }
        });
        return view;
    }

}
