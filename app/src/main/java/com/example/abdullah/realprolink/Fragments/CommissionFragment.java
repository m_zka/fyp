package com.example.abdullah.realprolink.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class CommissionFragment extends Fragment {

    public static String TAG = "FullScreenDialog";
    CommissionFragment me = this;

    //TextInputLayout variables
    private TextInputLayout textInputLayoutHousePrice;
    private TextInputLayout textInputLayoutCommissionPer;
    private TextView commissionAmt;

    //    EditText variables
    private EditText editTextHousePrice;
    private EditText editTextCommissionPer;

    //Button
    private Button calculateButton;
    private  Button resetButton;


    public static CommissionFragment newInstance() {
        return new CommissionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_commission,container,false);
        ((MainActivity)getActivity()).setActionBarTitle("Commission Calculator");
        //TextView Declaration
        textInputLayoutHousePrice = view.findViewById(R.id.input_edit_TextHousePrice);
        textInputLayoutCommissionPer = view.findViewById(R.id.input_layout_TextCommissionPer);

        commissionAmt = view.findViewById(R.id.commissionAmount);

//        EditText Declaration
        editTextHousePrice = view.findViewById(R.id.editTextHousePrice);
        editTextCommissionPer = view.findViewById(R.id.editTextCommissionPer);

        //Button
        calculateButton = view.findViewById(R.id.calculateCommission);
//        resetButton = view.findViewById(R.id.reset);


        editTextHousePrice.addTextChangedListener(new MyTextWatcher(editTextHousePrice));
        editTextCommissionPer.addTextChangedListener(new MyTextWatcher(editTextCommissionPer));


        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{

                    int houseval = Integer.parseInt( editTextHousePrice.getText().toString() );
                    double commissionval = Double.parseDouble(editTextCommissionPer.getText().toString());

                    commissionval =commissionval/100;

                    double total = houseval * commissionval;

                    commissionAmt.setText("" + Double.toString(total));


                }catch (NumberFormatException nfe){
                    nfe.printStackTrace();
                }

                submitForm();
            }
        });

//        resetButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                editTextCommissionPer.getText().clear();
//                editTextHousePrice.getText().clear();
//                editTextHousePrice.requestFocus();
//
//
//            }
//        });


        return view;
    }



    private void submitForm() {
        if (!validateHousePrice()) {
            return;
        }

        if (!validateCommissionPer()) {
            return;
        }

//        Toasty.success(getActivity(), "Success!", Toast.LENGTH_SHORT, true).show();
//        new GlideToast.makeToast(getActivity(),"Successfully Added ",GlideToast.LENGTHLONG,GlideToast.SUCCESSTOAST,GlideToast.BOTTOM,R.drawable.ic_skyline,"#008000").show();

    }

    private boolean validateHousePrice() {
        if (editTextHousePrice.getText().toString().trim().isEmpty()) {
            textInputLayoutHousePrice.setError(getString(R.string.error_message_HousePrice));
            requestFocus(editTextHousePrice);
            return false;
        } else {
            textInputLayoutHousePrice.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCommissionPer() {
        if (editTextCommissionPer.getText().toString().trim().isEmpty()) {
            textInputLayoutCommissionPer .setError(getString(R.string.error_message_CommissionPer));
            requestFocus(editTextCommissionPer);
            return false;
        } else {
            textInputLayoutCommissionPer .setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity) getContext()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.editTextHousePrice:
                    validateHousePrice();
                    break;
                case R.id.editTextCommissionPer:
                    validateCommissionPer();
                    break;
            }
        }
    }

}
