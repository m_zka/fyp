package com.example.abdullah.realprolink.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.abdullah.realprolink.Listener.RecyclerItemClickListener;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserProperty;
import com.example.abdullah.realprolink.R;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.List;

import io.paperdb.Paper;


/**
 * Created by Belal on 10/18/2017.
 */


public class UserPropertyAdapter extends RecyclerView.Adapter<UserPropertyAdapter.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<UserProperty> userProperties;
    private onItemClickListener mListener;
    public  interface onItemClickListener{

        void onItemClick(int position);
        void onDeleteClick(int position);


    }

    public void setOnItemClickListener(onItemClickListener listener){
        mListener=listener;
    }
    //getting the context and product list with constructor
    public UserPropertyAdapter(Context mCtx, List<UserProperty> productList) {
        this.mCtx = mCtx;
        this.userProperties = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.layout_userproperties, null);
        return new ProductViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, final int position) {
        //getting the product of the specified position
        UserProperty product = userProperties.get(position);



        //final String imageUrl = "http://movie.proglabs.org/images/"+product.getImage();

        // Glide.with(mCtx).load(imageUrl).into(holder.imageView);

        //binding the data with the viewholder views
//        holder.txt_id.setText(product.getPId());
       holder.txt_address.setText(product.getPLocation());
        //holder.txt_address.setText(product.getpCity());

        holder.txt_price.setText(product.getPPrice());
        holder.txt_beds.setText(product.getPBeds());
        holder.txt_baths.setText(product.getPBaths());
        holder.txt_area.setText(product.getPArea());
        Picasso.get().load("http://192.168.1.100:8080/FYP/api/uploads/".concat(product.getUrl())).into(holder.imageView);

//        holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(product.getImage()));

    }


    @Override
    public int getItemCount() {
        return userProperties.size();

//        int i= Integer.parseInt(String.valueOf(Paper.book().read("lsize")));
//        if(i == 0){
//            return 0;
//
//        }
//        else
//        {
//        }

    }




    public static class ProductViewHolder extends RecyclerView.ViewHolder  {

        TextView txt_price, txt_address, txt_beds, txt_baths, txt_area;
        ImageButton btnDelete;
        ImageView imageView;

        public ProductViewHolder(View itemView, final onItemClickListener listener) {
            super(itemView);


            txt_price = itemView.findViewById(R.id.myProperty_price);
            txt_address = itemView.findViewById(R.id.myProperty_address);
            txt_beds = itemView.findViewById(R.id.myProperty_beds);
            txt_baths = itemView.findViewById(R.id.myProperty_baths);
            txt_area = itemView.findViewById(R.id.myProperty_area);
            btnDelete=itemView.findViewById(R.id.myProperty_deleteProperty);
            imageView=itemView.findViewById(R.id.imgCard_myproperties);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(listener!=null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }

                    }
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }

                    }
                }

            });

        }


    }











}