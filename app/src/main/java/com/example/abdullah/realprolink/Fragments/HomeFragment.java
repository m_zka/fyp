package com.example.abdullah.realprolink.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.abdullah.realprolink.Activity.Login;
import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Fragments.working.SearchFragment;
import com.example.abdullah.realprolink.R;

import io.paperdb.Paper;

public class HomeFragment extends Fragment {



    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //  Action bar Title set
        ((MainActivity)getActivity()).setActionBarTitle("Home");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home,container,false);

        // declare
        ImageButton addproperty = view.findViewById(R.id.search);
        Button search = view.findViewById(R.id.searchbutton);

        // adding listeners
        addproperty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Paper.book().read("user_id")!=null){
                    FragmentTransaction fr =  getFragmentManager().beginTransaction();
                    fr.replace(R.id.fMain,new AddPropertyFragment());
                    fr.addToBackStack(null);
                    fr.commit();
                }
                else {

                    Intent intent = new Intent(getActivity(), Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);




                }

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fr =  getFragmentManager().beginTransaction();
                fr.replace(R.id.fMain,new SearchFragment());
                fr.addToBackStack(null);
                fr.commit();
            }
        });

        return view;
    }

}
