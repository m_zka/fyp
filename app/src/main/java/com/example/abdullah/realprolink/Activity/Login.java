package com.example.abdullah.realprolink.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.abdullah.realprolink.Fragments.LoginFragment;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Adapters.SectionStatePagerAdapter;

public class Login extends AppCompatActivity {


    private SectionStatePagerAdapter mSectionStatePagerAdapter;
    public ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginFragment fragment = LoginFragment.newInstance();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();

//       LinearLayout linearLayout=findViewById(R.id.newpassword1);
//        linearLayout.setVisibility(View.VISIBLE);


    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0 ){
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
