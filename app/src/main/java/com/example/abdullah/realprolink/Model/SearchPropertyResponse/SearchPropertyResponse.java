
package com.example.abdullah.realprolink.Model.SearchPropertyResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchPropertyResponse {

    @SerializedName("data")
    @Expose
    private List<SearchProperty> data = null;

    public List<SearchProperty> getData() {
        return data;
    }

    public void setData(List<SearchProperty> data) {
        this.data = data;
    }

}
