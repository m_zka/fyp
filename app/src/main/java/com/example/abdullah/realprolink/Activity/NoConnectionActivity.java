package com.example.abdullah.realprolink.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.abdullah.realprolink.Model.LoginResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.NetworkCheck;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoConnectionActivity extends AppCompatActivity {
Button refresh;
    private static NetworkCheck serviceGenerator = NetworkCheck.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);


        refresh=findViewById(R.id.btn_noConnection_retry);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Api api = RetrofitClient.getApiClient().create(Api.class);

                Call<LoginResponse> call = api.loginUser(Paper.book().read("Email").toString(), Paper.book().read("Password").toString());

                call.enqueue(new Callback<LoginResponse>() {

                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                        Log.i("Hello", "onResponse: ");
                        int code = response.code();

                        switch (code) {
                            case 200:
                                // Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                final Intent intent = new Intent(NoConnectionActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();

//                            Thread timer = new Thread(){
//                                public void run(){
//                                    try{
//                                        sleep(500);
//                                    } catch (InterruptedException e) {
//                                        e.printStackTrace();
//                                    }
//                                    finally {
//                                        startActivity(intent);
//                                        finish();
//                                    }
//                                }
//                            };
//                            timer.start();


                                break;

                            case 400:

//                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

//                                Toast.makeText(NoConnectionActivity.this, "Failed", Toast.LENGTH_SHORT).show();


                                break;

                        }
//                progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Log.i("Hello", "onResponse: " + t.getMessage());
//                        Toast.makeText(NoConnectionActivity.this, "No Internet", Toast.LENGTH_SHORT).show();



                    }

                });
            }
        });
    }
}
