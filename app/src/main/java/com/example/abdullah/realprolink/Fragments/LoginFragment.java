package com.example.abdullah.realprolink.Fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Model.LoginResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;
import com.jeevandeshmukh.glidetoastlib.GlideToast;

import es.dmoral.toasty.Toasty;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private TextInputLayout tvemail;
    private TextInputLayout tvpassword;
    private TextView forgetPassword;
    private Button loginButton;
    private Button createAccountButton;
    private EditText editTextEmail,editTextPassword;
        ProgressBar progressBar;
        LinearLayout linearLayout;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Paper.init(getContext());
        //final ProgressDialog progressDialog=new ProgressDialog(getContext());


        final View view = inflater.inflate(R.layout.fragment_login,container,false);
        tvemail = view.findViewById(R.id.input_edit_Text_signIn_Email);
        tvpassword = view.findViewById(R.id.input_edit_Text_signIn_Password);



//linearLayout=view.findViewById(R.id.sendOtpLayout);
//linearLayout.addView(progressBar);


//        progressBar=view.findViewById(R.id.prog_bar);
        forgetPassword = view.findViewById(R.id.tvForgotPass);
        loginButton = view.findViewById(R.id.loginButton);
        createAccountButton = view.findViewById(R.id.createaccountButton);
        editTextEmail =view.findViewById(R.id.editTextUname);
        editTextPassword=view.findViewById(R.id.editTextPassword);
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                // Check to see if the fragment is already showing.
                ForgetPasswordFragment forgetpassword = new ForgetPasswordFragment();
                fragmentTransaction.replace(R.id.fragment_container,forgetpassword);
                fragmentTransaction.addToBackStack("LoginFragment");
                fragmentTransaction.commit();
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                if (TextUtils.isEmpty(editTextEmail.getText())){
//
//                    Toast.makeText(getActivity(), "Email is required", Toast.LENGTH_SHORT).show();
////                    progressBar.setVisibility(View.INVISIBLE);
//                }
//                else if (TextUtils.isEmpty(editTextPassword.getText())){
//                    Toast.makeText(getActivity(), "Password is required", Toast.LENGTH_SHORT).show();
////                    progressBar.setVisibility(View.INVISIBLE);
//
//                }
//                else {
//                    progressBar.setVisibility(View.VISIBLE);
//                    signinUser();
//                    progressBar.setVisibility(View.INVISIBLE);
//
//                }


                if (!validateSignInEmail()) {
                    return;
                }
                else if (!validateSignInPassword()) {
                    return;
                }
                else{
                    signinUser();

                }







            }
        });

        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                // Check to see if the fragment is already showing.
                signupFragment signup = new signupFragment();
                fragmentTransaction.replace(R.id.fragment_container,signup);
                fragmentTransaction.addToBackStack("LoginFragment");
                fragmentTransaction.commit();











            }
        });


        return view;
    }

    private boolean validateSignInEmail() {
        String email = editTextEmail.getText().toString().trim();

        if (email.isEmpty()) {
            tvemail.setError(getString(R.string.error_message_SignIN_Email));
            requestFocus(editTextEmail);
            return false;
        }else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            tvemail.setError(getString(R.string.error_message_SignIN_Email_Valid));
            requestFocus(editTextEmail);
            return false;
        } else {
            tvemail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSignInPassword() {
        if (editTextPassword.getText().toString().trim().isEmpty()) {
            tvpassword .setError(getString(R.string.error_message_SignIN_Password));
            requestFocus(editTextPassword);
            return false;
        } else {
            tvpassword.setErrorEnabled(false);
        }

        return true;
    }



    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity) getContext()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_edit_Text_signIn_Email:
                    validateSignInEmail();
                    break;
                case R.id.input_edit_Text_signIn_Password:
                    validateSignInPassword();
                    break;
            }
        }
    }


    private void signinUser() {


        Api api = RetrofitClient.getApiClient().create(Api.class);

        Call<LoginResponse> call = api.loginUser(editTextEmail.getText().toString(), editTextPassword.getText().toString());

        call.enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                  Log.i("Hello", "onResponse: " );
                int code = response.code();

         //       progressBar.setVisibility(View.VISIBLE);
                switch (code) {
                    case 200:

                        Paper.book().write("Email",editTextEmail.getText().toString());
                        Paper.book().write("Password",editTextPassword.getText().toString());
                        Paper.book().write("user_id",response.body().getUserId());
                        Paper.book().write("user_type",response.body().getuserType());

//                        Paper.book().write("fname1",response.body().getFirstName());
  //                      Paper.book().write("lname1",response.body().getLastName());

                        Toast.makeText(getActivity(), String.valueOf(response.body().getuserType()), Toast.LENGTH_LONG).show();
                        // Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();




                        break;

                    case 400:

//                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

//                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                        Toasty.error(getActivity(), "Email or Password Incorrect", Toast.LENGTH_SHORT, true).show();



                        break;

                }
       //progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
            }

        });
    }

}
