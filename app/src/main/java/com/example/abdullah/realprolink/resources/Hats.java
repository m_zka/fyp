package com.example.abdullah.realprolink.resources;

import com.example.abdullah.realprolink.Model.Hat;
import com.example.abdullah.realprolink.R;

public class Hats {

    public static Hat[] getHats(){
        return SNAPBACKS;
    }

    public static final Hat SNAPBACK_BLACK = new Hat(R.drawable.snapback_black);
    public static final Hat SNAPBACK_CAMO = new Hat(R.drawable.snapback_camo);
    public static final Hat SNAPBACK_GREY = new Hat(R.drawable.snapback_grey);
    public static final Hat SNAPBACK_NAVY = new Hat(R.drawable.snapback_navy);
    public static final Hat SNAPBACK_RED = new Hat(R.drawable.snapback_red);
    public static final Hat SNAPBACK_TEAL = new Hat(R.drawable.snapback_teal);

    public static final Hat[] SNAPBACKS = {SNAPBACK_NAVY, SNAPBACK_BLACK, SNAPBACK_CAMO, SNAPBACK_GREY, SNAPBACK_RED, SNAPBACK_TEAL};



}