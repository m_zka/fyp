package com.example.abdullah.realprolink.Retrofit;






import com.example.abdullah.realprolink.Model.AgentListResponse.AgentsListResponse;
import com.example.abdullah.realprolink.Model.BidedPropertyResponse.BidedPropertiesResponse;
import com.example.abdullah.realprolink.Model.ChangekeytResponse;
import com.example.abdullah.realprolink.Model.DeletePropertyResponse;
import com.example.abdullah.realprolink.Model.ForgetResponse;
import com.example.abdullah.realprolink.Model.LoginResponse;
import com.example.abdullah.realprolink.Model.MaxBidResponse;
import com.example.abdullah.realprolink.Model.MyBidResponse;
import com.example.abdullah.realprolink.Model.Property.AddPropertyResponse;
import com.example.abdullah.realprolink.Model.PropertyBidResponse;
import com.example.abdullah.realprolink.Model.PropertyDetailResponse;
import com.example.abdullah.realprolink.Model.RegisterResponse;
import com.example.abdullah.realprolink.Model.URLResponse.URLResponse;
import com.example.abdullah.realprolink.Model.UpdateResponse;
import com.example.abdullah.realprolink.Model.UploadResponse;
import com.example.abdullah.realprolink.Model.UserInfoResponse;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserPropertyResponse;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface Api {
    @Multipart
    @POST("Api.php")

    Call<UploadResponse> uploadImage(

            @Part MultipartBody.Part filePart,

            @Part("user_id") RequestBody id

     );

    @Multipart
    @POST("Multi.php")

    Call<UploadResponse> uploadMultipleImage(

            @Part MultipartBody.Part filePart,

            @Part("p_title") RequestBody id

    );

    @POST("property/readall.php")
    Call<UserPropertyResponse> allProperties(

    );


    @POST("user/login.php")
    Call<LoginResponse> loginUser(
            @Query("user_email") String email,
            @Query("user_pwd") String password
    );


    @POST("user/rkeycheck.php")
    Call<ResponseBody> rKey(
            @Query("user_email") String email,
            @Query("recovery_key") String password
    );


    @FormUrlEncoded
    @POST("user/signup.php")
    Call<RegisterResponse> RegisterUser(
            @Field("user_email") String email,
            @Field("user_pwd") String password,
            @Field("first_name") String name,
            @Field("last_name") String last_name,
            @Field("user_city") String city,
            @Field("user_address") String address,
            @Field("user_mobile") String mobile,
            @Field("user_status") int user_status,
            @Field("user_type") int user_type,
            @Field("agent_request") int agent_request





            );




    @FormUrlEncoded
    @POST("user/update.php")
    Call<UpdateResponse> UpdateUser(
            @Field("user_id") String id,
            @Field("first_name") String name,
            @Field("last_name") String last_name,
            @Field("user_address") String address,
            @Field("user_mobile") String mobile,
            @Field("user_city") String city





    );



    @POST("user/read_user.php")
    Call<UserInfoResponse> ReadUser(
            @Query("user_id") String user_id

    );


    @POST("user/forget.php")
    Call<ForgetResponse> forget(
            @Query("user_email") String user_email,
            @Query("recovery_key") String recovery_key,
            @Query("user_pwd") String user_pwd

//            @Query("recovery_key") String recovery_key


    );


    @PUT("user/changekey.php")
    Call<ChangekeytResponse> changekey(
            @Query("user_email") String user_email
//            @Query("recovery_key") String recovery_key


    );


    @POST("property/my_properties.php")
    Call<UserPropertyResponse> PropertyUser(
            @Query("user_id") String usr_id

    );

    @POST("property/read_id.php")
    Call<PropertyDetailResponse> PropertyDetail(
            @Query("p_id") String property_id

    );




    @POST("property/minBid.php")
    Call<PropertyBidResponse> property_bid(
            @Query("p_id") String property_id

    );

    @FormUrlEncoded
    @POST("property/add_property.php")
    Call<AddPropertyResponse> Addproperty(

            @Field("user_id") String user_id,
            @Field("p_title") String title,
            @Field("p_location") String location,
            @Field("p_city") String city,
            @Field("p_area") String area,
            @Field("p_beds") String beds,
            @Field("p_baths") String baths,
            @Field("p_desc") String desc,
            @Field("p_photos") int photos,
            @Field("p_price") String price,
            @Field("p_type") int type,
            @Field("p_purpose") int purpose,

            @Field("property_email") String property_email,
            @Field("property_phone") String property_phone,
            @Field("longitude") double longitude,
            @Field("latitude") double latitude,
            @Field("min_bid") String min_bid



            );



    @FormUrlEncoded
    @POST("property/delete.php")
    Call<DeletePropertyResponse> deleteProperty(
            @Field("p_id") String p_id

    );


    @POST("property/myFavourites.php")
    Call<UserPropertyResponse> favourite(
            @Query("user_id") String user_id

    );


    @POST("bids/maxBid.php")
    Call<MaxBidResponse> maxBid(
            @Query("p_id") String p_id

    );

    @FormUrlEncoded
    @POST("property/addFavourites.php")
    Call<ResponseBody> addFavourites(
            @Field("user_id") String user_id,
            @Field("p_id") String p_id






    );


    @FormUrlEncoded
    @POST("property/deleteFav.php")
    Call<ResponseBody> delFavourites(
            @Field("user_id") String user_id,
            @Field("p_id") String p_id


    );



    @POST("bids/getBidedProp.php")
    Call<BidedPropertiesResponse> getBidProp(
            @Query("user_id") String user_id

    );


    @FormUrlEncoded
    @POST("bids/addbid.php")
    Call<ResponseBody> addBid(
            @Field("user_id") String user_id,
            @Field("p_id") String p_id,
            @Field("bid_amount") String bid_amount



            );
    @FormUrlEncoded
    @POST("bids/mybid.php")
    Call<MyBidResponse> myBid(
            @Field("user_id") String user_id,
            @Field("p_id") String p_id




    );

    @FormUrlEncoded
    @POST("bids/deleteBid.php")
    Call<ResponseBody> deleteBid(
            @Field("user_id") String user_id,
            @Field("p_id") String p_id




    );


    @POST("user/agents.php")
    Call<AgentsListResponse> agentsList(

    );

    @GET("search/search1.php")
    Call<UserPropertyResponse> search(
            @Query("p_beds") String p_beds,
            @Query("p_baths") String p_baths,
            @Query("p_price") String p_price,
            @Query("max_price") String max_price,
            @Query("p_area") String p_area,
            @Query("max_area") String max_area,
            @Query("p_city") String p_city,
            @Query("p_purpose") String p_purpose,
            @Query("p_type") String p_type

            );

    @POST("bids/getUri.php")
    Call<URLResponse> getUri(
            @Query("p_title") String p_title

    );

    @FormUrlEncoded
    @POST("bids/acceptBid.php")
    Call<ResponseBody> acceptBid(
            @Field("user_id") String user_id,
            @Field("p_id") String p_id


    );

    @FormUrlEncoded
    @POST("property/locate.php")
    Call<UserPropertyResponse> nearby(
            @Field("lng") String longitude,
            @Field("lat") String  latitude
            );


    @FormUrlEncoded
    @POST("bids/cancelBid.php")
    Call<ResponseBody> cancelBid(
            @Field("user_id") String user_id,
            @Field("p_id") String  p_id
    );

    @FormUrlEncoded
    @POST("user/agentRequest.php")
    Call<ResponseBody> requestAgent(
            @Field("user_id") String user_id

    );










}