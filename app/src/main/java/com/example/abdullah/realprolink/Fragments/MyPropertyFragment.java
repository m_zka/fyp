package com.example.abdullah.realprolink.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Adapters.UserPropertyAdapter;
import com.example.abdullah.realprolink.Adapters.ViewPagerAdapter;
import com.example.abdullah.realprolink.Model.DeletePropertyResponse;
import com.example.abdullah.realprolink.Model.MaxBidResponse;
import com.example.abdullah.realprolink.Model.PropertyBidResponse;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserProperty;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserPropertyResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyPropertyFragment extends Fragment    {

    List<UserProperty> propertyResponse;
    int size;



    private RecyclerView recyclerView;
    UserPropertyAdapter mAdapter;
    String maxBid=null;

    Context context;
    TextView price,address;
    Button btnDelete;
    String j;
    public MyPropertyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity)getActivity()).setActionBarTitle("My Properties");
       View view = inflater.inflate(R.layout.fragment_my_property, container, false);
        price = view.findViewById(R.id.myProperty_price);
        address = view.findViewById(R.id.myProperty_address);
        btnDelete=view.findViewById(R.id.myProperty_deleteProperty);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        propertyResponse =new ArrayList<UserProperty>();









        Api api = RetrofitClient.getApiClient().create(Api.class);

        Call<UserPropertyResponse> call = api.PropertyUser( Paper.book().read("user_id").toString());

        call.enqueue(new Callback<UserPropertyResponse>() {
            @Override
            public void onResponse(Call<UserPropertyResponse> call, final Response<UserPropertyResponse> response) {
                int code = response.code();

                switch (code) {
                    case 200:
                        Paper.book().write("lsize",1);
                        //  Toast.makeText(AddProperty.this, "Success", Toast.LENGTH_SHORT).show();
                        propertyResponse = response.body().getData();




                        //propertyResponse.get(1).getPId();

                        //get property ID from Response

                      mAdapter =new UserPropertyAdapter(getContext(),propertyResponse);
                       // UserPropertyAdapter adapter = new UserPropertyAdapter(getContext(), propertyResponse);
                        recyclerView.setAdapter(mAdapter);

                        mAdapter.setOnItemClickListener(new UserPropertyAdapter.onItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                int i=position;
                            propertyResponse.get(i).getPId();
                                j =  propertyResponse.get(i).getPId();

                                Toast.makeText(getActivity(), j, Toast.LENGTH_SHORT).show();
                                // Values set for Property Detail Fragment

                                Paper.book().write("d_price",propertyResponse.get(i).getPPrice());
                                Paper.book().write("d_title",propertyResponse.get(i).getPTitle());
                                Paper.book().write("d_address",propertyResponse.get(i).getPLocation());
                                Paper.book().write("d_beds",propertyResponse.get(i).getPBeds());
                                Paper.book().write("d_baths",propertyResponse.get(i).getPBaths());
                                Paper.book().write("d_pid",propertyResponse.get(i).getPId());
                                Paper.book().write("d_purpose",propertyResponse.get(i).getpPurpose());
                                Paper.book().write("d_type",propertyResponse.get(i).getpType());
                                Paper.book().write("d_desc",propertyResponse.get(i).getPDesc());
                                Paper.book().write("d_status",propertyResponse.get(i).getPStatus());


                                Paper.book().write("d_area",propertyResponse.get(i).getPArea());
                                Paper.book().write("d_long",propertyResponse.get(i).getLongitude());
                                Paper.book().write("d_lat",propertyResponse.get(i).getLatitude());



                                final Api api = RetrofitClient.getApiClient().create(Api.class);

                                Call<MaxBidResponse> call1 = api.maxBid(j);

                                call1.enqueue(new Callback<MaxBidResponse>() {
                                    @Override
                                    public void onResponse(Call<MaxBidResponse> call1, Response<MaxBidResponse> response) {

                                           if(response.body().getUserId()!=null){
                                               maxBid = response.body().getBidAmount();
                                               Paper.book().write("maxbid", response.body().getBidAmount());
                                               Paper.book().write("check","0");
                                           }



                                        else{


                                            Call<PropertyBidResponse> call4 = api.property_bid(j);


                                            call4.enqueue(new Callback<PropertyBidResponse>() {
                                                @Override
                                                public void onResponse(Call<PropertyBidResponse> call, Response<PropertyBidResponse> response) {
                                                    if(response.body().getMinBid()==null){

                                                        Paper.book().write("maxbid", response.body().getMinBid());
                                                        Paper.book().write("check","1");

                                                    }


                                                }

                                                @Override
                                                public void onFailure(Call<PropertyBidResponse> call, Throwable t) {

                                                }
                                            });



                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<MaxBidResponse> call, Throwable t) {

                                    }
                                });




                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.fMain,new PropertyDetails());
                                ft.addToBackStack(null);
                                ft.commit();
                            }

                            @Override
                            public void onDeleteClick(int position) {
                              int i=position;


                                deleteProperty(i);
                            }
                        });
//                        recyclerView.addOnItemTouchListener(
//
//                                new RecyclerItemClickListener(context, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
//                                    @Override public void onItemClick(View view, int position) {
//                                        int i = position;
//                                        j =  propertyResponse.get(i).getPId();
//
//                                        Toast.makeText(getActivity(), j, Toast.LENGTH_SHORT).show();
//                                      //  Toast.makeText(getActivity(), Paper.book().read("p_idd").toString(), Toast.LENGTH_SHORT).show();
//
//                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
//                                        ft.replace(R.id.fMain,new CompareFragment());
//                                        ft.addToBackStack(null);
//                                        ft.commit();
//
//
//
//
//
//                                    }
//
//                                    @Override public void onLongItemClick(View view, int position) {
//                                        // do whatever
//                                    }
//                                })
//                        );
                        break;

                    case 400:
                      Paper.book().write("lsize",0);
                        break;
                }
//            progressBar1.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UserPropertyResponse> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
            }
        });









        return  view;
    }
public void deleteProperty(int position){

      String p_id=  propertyResponse.get(position).getPId();

    Toast.makeText(getActivity(), p_id, Toast.LENGTH_SHORT).show();

    Api api = RetrofitClient.getApiClient().create(Api.class);
    Call<DeletePropertyResponse>call=  api.deleteProperty(p_id);


    call.enqueue(new Callback<DeletePropertyResponse>() {
        @Override
        public void onResponse(Call<DeletePropertyResponse> call, Response<DeletePropertyResponse> response) {

            int code = response.code();

            switch (code) {
                case 200:
                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    break;

                case 400:
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        @Override
        public void onFailure(Call<DeletePropertyResponse> call, Throwable t) {

        }
    });
        propertyResponse.remove(position);
        mAdapter.notifyDataSetChanged();
    }

    public List<UserProperty> getPropertyResponse() {
        return propertyResponse;
    }
}
