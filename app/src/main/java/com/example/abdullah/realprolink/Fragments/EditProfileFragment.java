package com.example.abdullah.realprolink.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.ProfileActivity;
import com.example.abdullah.realprolink.Model.UpdateResponse;
import com.example.abdullah.realprolink.Model.UploadResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.File;

import io.paperdb.Paper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends DialogFragment {


   final Api api = RetrofitClient.getApiClient().create(Api.class);
    public static String TAG = "FullScreenDialog";
    EditProfileFragment me = this;

    //TextInputLayout variables
    private TextInputLayout textInputLayoutName;
    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutLocation;
    private TextInputLayout textInputLayoutAddress;
    private TextInputLayout textInputLayoutPhone;

    //EditText variables
    private EditText editTextFirstName;
    private EditText editTextLastName;

    private EditText editTextEmail;
    private EditText editTextLocation;
    private EditText editTextAddress;
    private EditText editTextPhone;

    private Button updateButton;
    private AppCompatImageButton uploadButton;
    CircularImageView imageView;
    TextView agentRequest;

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getActivity().getPackageName()));
            getActivity().finish();
            startActivity(intent);
            return;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            //the image URI
            Uri selectedImage = data.getData();

            //calling the upload file method after choosing the file
            uploadFile(selectedImage, "My Image",Paper.book().read("user_id").toString());
        }
    }

    private void uploadFile(Uri fileUri, String desc,String id) {

        //creating a file
        File file = new File(getRealPathFromURI(fileUri));
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        //creating request body for file
        RequestBody requestFile = RequestBody.create(MediaType.parse(getActivity().getApplicationContext().getContentResolver().getType(fileUri)), file);
        RequestBody descBody = RequestBody.create(MediaType.parse("text/plain"), desc);
        RequestBody userBody = RequestBody.create(MediaType.parse("text/plain"), id);




        //creating retrofit object


        //creating our api


        //creating a call and calling the upload image method
        Call<UploadResponse> call = api.uploadImage(filePart,userBody);

        //finally performing the call
        call.enqueue(new Callback<UploadResponse>() {
            @Override
            public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
                //Toast.makeText(getContext(), "File Uploaded Successfully...", Toast.LENGTH_LONG).show();
                Intent i = new Intent(getActivity(), ProfileActivity.class);
                startActivity(i);
//                if (response.body().getMessage()=="File Uploaded Successfullly") {
//                } else {
//                    Toast.makeText(getContext(), "Some error occurred...", Toast.LENGTH_LONG).show();
//                }
            }

            @Override
            public void onFailure(Call<UploadResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile,container,false);


agentRequest=view.findViewById(R.id.requestAgent);
        //TextView Declaration
        textInputLayoutName = view.findViewById(R.id.input_edit_TextName);
        textInputLayoutEmail = view.findViewById(R.id.input_layout_TextEmail);
        textInputLayoutLocation = view.findViewById(R.id.input_layout_TextLocation);
        textInputLayoutAddress = view.findViewById(R.id.input_layout_TextAddress);
        textInputLayoutPhone = view.findViewById(R.id.input_layout_TextPhone);

        //EditText Declaration
        editTextFirstName = view.findViewById(R.id.editTextfirstName);
        editTextLastName = view.findViewById(R.id.editTextLastName);

        imageView=view.findViewById(R.id.img_edit);
        Picasso.get().load("http://192.168.1.100:8080/FYP/api/uploads/".concat(Paper.book().read("p_url").toString())).into(imageView);


        editTextEmail = view.findViewById(R.id.editTextEmail);
        editTextLocation = view.findViewById(R.id.editTextLocation);
        editTextAddress = view.findViewById(R.id.editTextAddress);
        editTextPhone = view.findViewById(R.id.editTextPhone);

        updateButton = view.findViewById(R.id.updatebutton);
        uploadButton =view.findViewById(R.id.uploadbutton);
        editTextFirstName.setText((CharSequence) Paper.book().read("first_name"));
        editTextLastName.setText((CharSequence) Paper.book().read("last_name"));

        editTextEmail.setText((CharSequence) Paper.book().read("Email"));
        editTextEmail.setFocusable(false);
        editTextLocation.setText((CharSequence) Paper.book().read("city"));
        editTextAddress.setText((CharSequence) Paper.book().read("address"));
        editTextPhone.setText((CharSequence) Paper.book().read("phone"));


        editTextFirstName.addTextChangedListener(new MyTextWatcher(editTextFirstName));
        editTextLastName.addTextChangedListener(new MyTextWatcher(editTextLastName));

        editTextEmail.addTextChangedListener(new MyTextWatcher(editTextEmail));
        editTextLocation.addTextChangedListener(new MyTextWatcher(editTextLocation));
        editTextAddress.addTextChangedListener(new MyTextWatcher(editTextAddress));
        editTextPhone.addTextChangedListener(new MyTextWatcher(editTextPhone));


        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//              android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
//              android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//              fragmentTransaction.remove(me);
//              fragmentTransaction.commit();
                Intent i= new Intent(getActivity(),ProfileActivity.class);
                startActivity(i);
            }
        });
        toolbar.setTitle("Edit Profile");

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
                update();
            }
        });
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(i, 100);

            }
        });

        if(Integer.parseInt(Paper.book().read("user_type").toString()) == 1){

            agentRequest.setVisibility(View.GONE);

        }


        agentRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> call1 = api.requestAgent(Paper.book().read("user_id").toString()

                );


                    call1.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Toast.makeText(getContext(),"Your request is sent for Approval",Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });



            }
        });


        return view;
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }
    private void update() {




        int status=0;
        Call<UpdateResponse> call = api.UpdateUser(
                Paper.book().read("user_id").toString(),
                editTextFirstName.getText().toString(),
                editTextLastName.getText().toString(),
                editTextAddress.getText().toString(),
                editTextPhone.getText().toString(),
                editTextLocation.getText().toString()





        );

        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                int code = response.code();

                switch (code) {
                    case 200:
                       Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();

                        // Check to see if the fragment is already showing.
//                        FragmentTransaction ft=getFragmentManager().beginTransaction();
//                        ft.replace(R.id.fragment_userProfile, new UserProfileFragment());
//                        ft.commit();

                        Intent i = new Intent(getActivity(), ProfileActivity.class);
                        startActivity(i);

                        break;

                    case 400:
                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                        break;
                }
//            progressBar1.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateFirstName()) {
            return;
        }
        if (!validateLastName()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }

        if (!validateLocation()) {
            return;
        }

        if (!validateAddress()) {
            return;
        }
        if (!validatePhone()) {
            return;
        }

       // Toast.makeText(getActivity().getApplicationContext(), "Updated Successfully! ", Toast.LENGTH_SHORT).show();
    }

    private boolean validateFirstName() {
        if (editTextFirstName.getText().toString().trim().isEmpty()) {
            textInputLayoutName.setError(getString(R.string.error_message_name));
            requestFocus(editTextFirstName);
            return false;
        } else  {
            textInputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLastName() {
        if (editTextLastName.getText().toString().trim().isEmpty()) {
            textInputLayoutName.setError(getString(R.string.error_message_name));
            requestFocus(editTextLastName);
            return false;
        } else  {
            textInputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = editTextEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            textInputLayoutEmail.setError(getString(R.string.error_message_email));
            requestFocus(editTextEmail);
            return false;
        } else {
            textInputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLocation() {
        if (editTextLocation.getText().toString().trim().isEmpty()) {
            textInputLayoutLocation.setError(getString(R.string.error_message_Location));
            requestFocus(editTextLocation);
            return false;
        } else {
            textInputLayoutLocation.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAddress() {
        if (editTextAddress.getText().toString().trim().isEmpty()) {
            textInputLayoutAddress.setError(getString(R.string.error_message_Address));
            requestFocus(editTextAddress);
            return false;
        } else {
            textInputLayoutAddress.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePhone() {
        if (editTextPhone.getText().toString().trim().isEmpty()) {
            textInputLayoutPhone.setError(getString(R.string.error_message_Phone));
            requestFocus(editTextPhone);
            return false;
        } else {
            textInputLayoutPhone.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity) getContext()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.editTextfirstName:
                    validateFirstName();
                    break;
                case R.id.editTextLastName:
                    validateLastName();
                    break;

                case R.id.editTextEmail:
                    validateEmail();
                    break;
                case R.id.editTextLocation:
                    validateLocation();
                    break;
                case R.id.editTextAddress:
                    validateAddress();
                    break;
                case R.id.editTextPhone:
                    validatePhone();
                    break;
            }
        }
    }
}

