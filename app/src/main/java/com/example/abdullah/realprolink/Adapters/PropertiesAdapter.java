package com.example.abdullah.realprolink.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserProperty;
import com.example.abdullah.realprolink.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.paperdb.Paper;

public class PropertiesAdapter extends RecyclerView.Adapter<PropertiesAdapter.ProductViewHolder> {





    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<UserProperty> userProperties;
    private PropertiesAdapter.onItemClickListener mListener;
    public  interface onItemClickListener{

        void onItemClick(int position);
        void onFavClick(int position);


    }

    public void setOnItemClickListener(PropertiesAdapter.onItemClickListener listener){
        mListener=listener;
    }
    //getting the context and product list with constructor
    public PropertiesAdapter(Context mCtx, List<UserProperty> productList) {
        this.mCtx = mCtx;
        this.userProperties = productList;
    }

    @Override
    public PropertiesAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.cardview_properties, null);
        return new PropertiesAdapter.ProductViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(PropertiesAdapter.ProductViewHolder holder, final int position) {
        //getting the product of the specified position
        UserProperty product = userProperties.get(position);



        //final String imageUrl = "http://movie.proglabs.org/images/"+product.getImage();

        // Glide.with(mCtx).load(imageUrl).into(holder.imageView);

        //binding the data with the viewholder views
//        holder.txt_id.setText(product.getPId());
     holder.txt_address.setText(product.getPLocation());

      //  holder.txt_address.setText(product.getpCity());

        holder.txt_price.setText(product.getPPrice());
        holder.txt_beds.setText(product.getPBeds());
        holder.txt_baths.setText(product.getPBaths());
        holder.txt_area.setText(product.getPArea());

        Picasso.get().load("http://192.168.1.100:8080/FYP/api/uploads/".concat(product.getUrl())).into(holder.imageView);

//        holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(product.getImage()));

    }


    @Override
    public int getItemCount() {

        return userProperties.size();
//        int i= Integer.parseInt(String.valueOf(Paper.book().read("lsize")));
//        if(i == 0){
//            return 0;
//
//        }
//        else
//        {
//
//        }

    }




    public static class ProductViewHolder extends RecyclerView.ViewHolder  {

        TextView txt_price, txt_address, txt_beds, txt_baths, txt_area;
        ImageView imageView;
        ImageButton btnDelete;

        public ProductViewHolder(View itemView, final PropertiesAdapter.onItemClickListener listener) {
            super(itemView);


            txt_price = itemView.findViewById(R.id.A_Property_price);
            txt_address = itemView.findViewById(R.id.A_Property_address);
            txt_beds = itemView.findViewById(R.id.A_Property_beds);
            txt_baths = itemView.findViewById(R.id.A_Property_baths);
            txt_area = itemView.findViewById(R.id.A_Property_area);
            btnDelete=itemView.findViewById(R.id.A_Property_image);
            imageView=itemView.findViewById(R.id.CardImg_allProperties);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(listener!=null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onFavClick(position);
                        }

                    }
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }

                    }
                }

            });

        }


    }














}
