package com.example.abdullah.realprolink.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.abdullah.realprolink.Model.RegisterResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class signupFragment extends Fragment {

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$");


    private static final Pattern Alphabet_PATTERN =
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
//                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{2,15}" +               //at least 4 characters
                    "$");

    private static final Pattern Address_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
//                    "(?=\\S+$)" +           //no white spaces
                    ".{10,100}" +               //at least 4 characters
                    "$");

    private static final Pattern Phone_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
//                    "(?=.*[a-zA-Z])" +      //any letter
//                    "(?=.*[+])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{11,13}" +               //at least 4 characters
                    "$");


    private Button cAccountButton;
    private TextInputLayout fnametv,lastnametv,emailtv,passwordtv,addresstv,citytv,phonetv;

    EditText editTextFName,editTextLName,editTextEmail,editTextPwd,editTextAddress,editTextCity,editTextPhone;

    public signupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_signup,container,false);

        fnametv = view.findViewById(R.id.input_edit_Text_signUP_FNAME);
        lastnametv = view.findViewById(R.id.input_edit_Text_signUP_LNAME);
        emailtv = view.findViewById(R.id.input_edit_Text_signUP_EMAIL);
        passwordtv = view.findViewById(R.id.input_edit_Text_signUP_PASSWORD);
        addresstv = view.findViewById(R.id.input_edit_Text_signUP_PASSWORD);
        citytv = view.findViewById(R.id.input_edit_Text_signUP_ADDRESS);
        phonetv = view.findViewById(R.id.input_edit_Text_signUP_PHONENO);

        editTextFName=view.findViewById(R.id.editTextFName);
        editTextLName=view.findViewById(R.id.editTextLName);

        editTextAddress=view.findViewById(R.id.editAddress);
        editTextPhone= view.findViewById(R.id.editPhoneno);
        editTextEmail=view.findViewById(R.id.editEmail);
        editTextCity=view.findViewById(R.id.editCity);
        editTextPwd=view.findViewById(R.id.editPassword);


        cAccountButton = view.findViewById(R.id.createAccountBut);

        cAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //API call
//                if (TextUtils.isEmpty(editTextFName.getText())){
//                    Toast.makeText(getActivity(), "Name is required", Toast.LENGTH_SHORT).show();
////                    progressBar1.setVisibility(View.INVISIBLE);
//                }
//                else if (TextUtils.isEmpty(editTextEmail.getText())){
//                    Toast.makeText(getActivity(), "Email is required", Toast.LENGTH_SHORT).show();
////                    progressBar1.setVisibility(View.INVISIBLE);
//
//                }
//                else if (TextUtils.isEmpty(editTextPwd.getText())){
//                    Toast.makeText(getActivity(), "Password is required", Toast.LENGTH_SHORT).show();
////                    progressBar1.setVisibility(View.INVISIBLE);
//
//                }
//                else if (TextUtils.isEmpty(editTextPhone.getText())){
//                    Toast.makeText(getActivity(), "Number is required", Toast.LENGTH_SHORT).show();
////                    progressBar1.setVisibility(View.INVISIBLE);
//
//                }
//                else if (TextUtils.isEmpty(editTextCity.getText())){
//                    Toast.makeText(getActivity(), "Country is required", Toast.LENGTH_SHORT).show();
////                    progressBar1.setVisibility(View.INVISIBLE);
//
//                }
//                else if (TextUtils.isEmpty(editTextAddress.getText())){
//                    Toast.makeText(getActivity(), "Gender is required", Toast.LENGTH_SHORT).show();
////                    progressBar1.setVisibility(View.INVISIBLE);
//
//                }
//
//
//                else {
//                    signupUser();
//
//                }

                if (!validateFirstName()) {
                    return;
                }else if (!validateLastName()) {
                    return;
                }else if (!validateSignUpEmail()) {
                    return;
                }else if (!validateSignUpPassword()) {
                    return;
                }else if (!validateSignUpAddress()) {
                    return;
                }else if (!validateSignUpCity()) {
                    return;
                }else if (!validateSignUpPhone()) {
                    return;
                }


                else {
                    signupUser();

                }



            }
        });
        return view;
    }


    private boolean validateFirstName() {
        String firstName = editTextFName.getText().toString().trim();

        if (firstName.isEmpty()) {
            fnametv.setError(getString(R.string.error_message_SignUP_FNAME));
            requestFocus(editTextFName);
            return false;
        } else if(!Alphabet_PATTERN.matcher(firstName).matches()){
            fnametv.setError(getString(R.string.error_message_SignUP_FNAME_Vaild));
            requestFocus(editTextFName);
            return false;
        }else {
            fnametv.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLastName() {
        String LastName = editTextLName.getText().toString().trim();

        if (LastName.isEmpty()) {
            lastnametv.setError(getString(R.string.error_message_SignUP_LNAME));
            requestFocus(editTextLName);
            return false;
        } else if(!Alphabet_PATTERN.matcher(LastName).matches()){
            lastnametv.setError(getString(R.string.error_message_SignUP_LNAME_Vaild));
            requestFocus(editTextLName);
            return false;
        } else {
            lastnametv.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSignUpEmail() {
        String email = editTextEmail.getText().toString().trim();

        if (email.isEmpty()){
            emailtv.setError(getString(R.string.error_message_SignUP_EMAIL));
            requestFocus(editTextEmail);
            return false;
        } else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailtv.setError(getString(R.string.error_message_SignUP_Valid_EMAIL));
            requestFocus(editTextEmail);
            return false;
        }else {
            emailtv.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSignUpPassword() {
        String password = editTextPwd.getText().toString().trim();

        if (password.isEmpty()){
            passwordtv.setError(getString(R.string.error_message_SignUP_PASSWORD));
            requestFocus(editTextPwd);
            return false;
        } else if(!PASSWORD_PATTERN.matcher(password).matches()){
            passwordtv.setError(getString(R.string.error_message_SignUP_PASSWORD_WEAK));
            requestFocus(editTextPwd);
            return false;
        }else {
            passwordtv.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSignUpAddress() {
        String address = editTextAddress.getText().toString().trim();

        if (address.isEmpty()) {
            addresstv.setError(getString(R.string.error_message_SignUP_ADDRESS));
            requestFocus(editTextAddress);
            return false;
        }else if(!Address_PATTERN.matcher(address).matches()){
            addresstv.setError(getString(R.string.error_message_SignUP_ADDRESS_Valid));
            requestFocus(editTextAddress);
            return false;
        } else {
            addresstv.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSignUpCity() {
        String city = editTextCity.getText().toString().trim();

        if (city.isEmpty()) {
            citytv.setError(getString(R.string.error_message_SignUP_CITY));
            requestFocus(editTextCity);
            return false;
        } else if(!Alphabet_PATTERN.matcher(city).matches()){
            citytv.setError(getString(R.string.error_message_SignUP_CITY_Vaild));
            requestFocus(editTextCity);
            return false;
        }else {
            citytv.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSignUpPhone() {
        String phone = editTextPhone.getText().toString().trim();

        if (phone.isEmpty()) {
            phonetv.setError(getString(R.string.error_message_SignUP_Phone));
            requestFocus(editTextPhone);
            return false;
        } else if(!Phone_PATTERN.matcher(phone).matches()){
            phonetv.setError(getString(R.string.error_message_SignUP_Phone_Valid));
            requestFocus(editTextPhone);
            return false;
        }else {
            phonetv.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((Activity) getContext()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.editTextFName:
                    validateFirstName();
                    break;
                case R.id.editTextLName:
                    validateLastName();
                    break;
                case R.id.editEmail:
                    validateSignUpEmail();
                    break;
                case R.id.editPassword:
                    validateSignUpPassword();
                    break;
                case R.id.editAddress:
                    validateSignUpAddress();
                    break;
                case R.id.editCity:
                    validateSignUpPassword();
                    break;
                case R.id.editPhoneno:
                    validateSignUpPhone();
                    break;
            }
        }
    }




    private void signupUser() {
        Api api = RetrofitClient.getApiClient().create(Api.class);
int status=0;
        Call<RegisterResponse> call = api.RegisterUser(
                editTextEmail.getText().toString(),
                editTextPwd.getText().toString(),
                editTextFName.getText().toString(),
                editTextLName.getText().toString(),
                editTextCity.getText().toString(),
                editTextAddress.getText().toString(),
                editTextPhone.getText().toString(),
                status,status,status


        );

        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                int code = response.code();

                switch (code) {
                    case 200:
//                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                        Toasty.success(getActivity(), "Account Successfully Created !", Toast.LENGTH_SHORT, true).show();

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        // Check to see if the fragment is already showing.
                        LoginFragment login = new LoginFragment();
                        fragmentTransaction.replace(R.id.fragment_container,login);
                        fragmentTransaction.addToBackStack("LoginFragment");
                        fragmentTransaction.commit();
                        break;

                    case 400:
//                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                        Toasty.error(getActivity(), "Email already exist.", Toast.LENGTH_SHORT, true).show();
//
                        break;
                }
//            progressBar1.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
            }
        });




    }

}
