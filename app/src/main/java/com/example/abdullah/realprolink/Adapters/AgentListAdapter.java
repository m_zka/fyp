package com.example.abdullah.realprolink.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abdullah.realprolink.Model.AgentListResponse.AgentsResponse;
import com.example.abdullah.realprolink.Model.BidedPropertyResponse.BidedPropertites;
import com.example.abdullah.realprolink.Model.UserPropertyResponse.UserProperty;
import com.example.abdullah.realprolink.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.paperdb.Paper;

public class AgentListAdapter extends RecyclerView.Adapter<AgentListAdapter.ProductViewHolder> {





    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<AgentsResponse> agentList;
    private AgentListAdapter.onItemClickListener mListener;
    public  interface onItemClickListener{

        void onItemClick(int position);
        void onDeleteClick(int position);


    }

    public void setOnItemClickListener(AgentListAdapter.onItemClickListener listener){
        mListener=listener;
    }
    //getting the context and product list with constructor
    public AgentListAdapter(Context mCtx, List<AgentsResponse> productList) {
        this.mCtx = mCtx;
        this.agentList = productList;
    }

    @Override
    public AgentListAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.cardview_agents, null);
        return new AgentListAdapter.ProductViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(AgentListAdapter.ProductViewHolder holder, final int position) {
        //getting the product of the specified position
        AgentsResponse product = agentList.get(position);




        holder.txt_name.setText(product.getFirstName().concat(" ").concat(product.getLastName()));
        holder.txt_address.setText(product.getUserAddress().concat(" ,").concat(product.getCity()));
        holder.txt_phone.setText(product.getPhone());
        Picasso.get().load("http://192.168.1.100:8080/FYP/api/uploads/".concat(product.getUrl())).into(holder.imageView);

      //  Picasso.get().load("http://192.168.137.73:8080/FYP/api/uploads/1564570334413.PNG").into(holder.imageView);




    }


    @Override
    public int getItemCount() {
        return agentList.size();

    }




    public static class ProductViewHolder extends RecyclerView.ViewHolder  {

        TextView txt_name,txt_address,txt_phone;
        ImageView imageView;
        ImageButton btnDelete;

        public ProductViewHolder(View itemView, final AgentListAdapter.onItemClickListener listener) {
            super(itemView);

//
            txt_name = itemView.findViewById(R.id.agent_name);
            txt_address = itemView.findViewById(R.id.agent_address);
             txt_phone= itemView.findViewById(R.id.agent_phone);
             imageView=itemView.findViewById(R.id.CardImg_agent);





        }


    }














}
