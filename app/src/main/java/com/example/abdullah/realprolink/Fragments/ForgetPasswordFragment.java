package com.example.abdullah.realprolink.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdullah.realprolink.Email.SendMail;
import com.example.abdullah.realprolink.Model.ChangekeytResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForgetPasswordFragment extends Fragment {

    private Button forgetPasswordButton;



    public ForgetPasswordFragment() {
        // Required empty public constructor
    }
String recovery_key;

AutoCompleteTextView email;
    TextView password;
    EditText recovery;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_forget_password,container,false);


        email=view.findViewById(R.id.forget_email);



        forgetPasswordButton = view.findViewById(R.id.btnReset);

        forgetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





                if (TextUtils.isEmpty(email.getText())){

                    Toast.makeText(getActivity(), "Email is required", Toast.LENGTH_SHORT).show();
//                    progressBar.setVisibility(View.INVISIBLE);
                }

                else {
                    forget();



                }




            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void forget() {


        Api api1 = RetrofitClient.getApiClient().create(Api.class);
        Api api2 = RetrofitClient.getApiClient().create(Api.class);


//        Call<ForgetResponse> call = api1.forget(
//                email.getText().toString()
//
//
//        );
        Call<ChangekeytResponse> call1 = api2.changekey(
                email.getText().toString()


        );







        call1.enqueue(new Callback<ChangekeytResponse>() {
            @Override
            public void onResponse(Call<ChangekeytResponse> call, Response<ChangekeytResponse> response) {
                int code = response.code();

                switch (code) {
                    case 200:
                        //Toast.makeText(getActivity(), response.body().getUserId(), Toast.LENGTH_SHORT).show();
//                         forget_email=response.body().getUserId();

                        recovery_key=response.body().getRecoveryKey();
                        Paper.book().write("forget_key",response.body().getRecoveryKey());

                        sendEmail();

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        // Check to see if the fragment is already showing.

                        PassRecoveryFragment passwordRecovery = new PassRecoveryFragment();
                        fragmentTransaction.replace(R.id.fragment_container,passwordRecovery);
                        fragmentTransaction.addToBackStack("ForgetPasswordFragment");
                        fragmentTransaction.commit();

                        Paper.book().write("forget_email",email.getText().toString());

                        break;

                    case 400:
                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                        break;
                }
//            progressBar1.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ChangekeytResponse> call, Throwable t) {
                Log.i("Hello", "onResponse: " + t.getMessage());
            }
        });





    }
    private void sendEmail() {
        //Getting content for email



        //Creating SendMail object
        SendMail sm = new SendMail(getContext(), email.getText().toString(), "Recovery Key", recovery_key);

        //Executing sendmail to send email
        sm.execute();
    }
}
