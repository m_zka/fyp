
package com.example.abdullah.realprolink.Model.UserPropertyResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProperty {

    @SerializedName("p_id")
    @Expose
    private String pId;

    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("p_title")
    @Expose
    private String pTitle;
    @SerializedName("p_location")
    @Expose
    private String pLocation;
    @SerializedName("p_area")
    @Expose
    private String pArea;
    @SerializedName("p_beds")
    @Expose
    private String pBeds;
    @SerializedName("p_baths")
    @Expose
    private String pBaths;
    @SerializedName("p_desc")
    @Expose
    private String pDesc;
    @SerializedName("p_photos")
    @Expose
    private String pPhotos;
    @SerializedName("p_status")
    @Expose
    private String pStatus;
    @SerializedName("p_price")
    @Expose
    private String pPrice;
    @SerializedName("p_date")
    @Expose
    private String pDate;
    @SerializedName("p_city")
    @Expose
    private String pCity;
    @SerializedName("p_purpose")
    @Expose
    private String pPurpose;

    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("latitude")
    @Expose
    private String latitude;



    @SerializedName("p_type")
    @Expose
    private String pType;

    @SerializedName("url")
    @Expose
    private String url;




    public String getLongitude() {
        return longitude;
    }
    public String getLatitude() {
        return latitude;
    }

    public String getUrl() {
        return url;
    }

    public String getpUserID() {
        return user_id;
    }

    public String getpPurpose() {
        return pPurpose;
    }
    public String getpType() {
        return pType;
    }
    public String getpCity() {
        return pCity;
    }

    public String getPId() {
        return pId;
    }

    public void setPId(String pId) {
        this.pId = pId;
    }

    public String getPTitle() {
        return pTitle;
    }

    public void setPTitle(String pTitle) {
        this.pTitle = pTitle;
    }

    public String getPLocation() {
        return pLocation;
    }

    public void setPLocation(String pLocation) {
        this.pLocation = pLocation;
    }

    public String getPArea() {
        return pArea;
    }

    public void setPArea(String pArea) {
        this.pArea = pArea;
    }

    public String getPBeds() {
        return pBeds;
    }

    public void setPBeds(String pBeds) {
        this.pBeds = pBeds;
    }

    public String getPBaths() {
        return pBaths;
    }

    public void setPBaths(String pBaths) {
        this.pBaths = pBaths;
    }

    public String getPDesc() {
        return pDesc;
    }

    public void setPDesc(String pDesc) {
        this.pDesc = pDesc;
    }

    public String getPPhotos() {
        return pPhotos;
    }

    public void setPPhotos(String pPhotos) {
        this.pPhotos = pPhotos;
    }

    public String getPStatus() {
        return pStatus;
    }

    public void setPStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public String getPPrice() {
        return pPrice;
    }

    public void setPPrice(String pPrice) {
        this.pPrice = pPrice;
    }

    public String getPDate() {
        return pDate;
    }

    public void setPDate(String pDate) {
        this.pDate = pDate;
    }

}
