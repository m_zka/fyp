package com.example.abdullah.realprolink.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdullah.realprolink.Model.LoginResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity  {

    private ImageView iv;   //splash screen image
    Handler handler = new Handler();
    Runnable runnable;
    int delay = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        iv = (ImageView) findViewById(R.id.splash_logo);
        Paper.init(this);
        Animation myanim = AnimationUtils.loadAnimation(this,R.anim.mytransition);
        iv.startAnimation(myanim);


        if(Paper.book().read("Email")!=null) {

            Api api = RetrofitClient.getApiClient().create(Api.class);

            Call<LoginResponse> call = api.loginUser(Paper.book().read("Email").toString(), Paper.book().read("Password").toString());

            call.enqueue(new Callback<LoginResponse>() {

                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                    Log.i("Hello", "onResponse: ");
                    int code = response.code();

                    switch (code) {
                        case 200:

                            Paper.book().write("user_id",response.body().getUserId());
                            Paper.book().write("user_type",response.body().getuserType());



                            // Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            final Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();

//                            Thread timer = new Thread(){
//                                public void run(){
//                                    try{
//                                        sleep(500);
//                                    } catch (InterruptedException e) {
//                                        e.printStackTrace();
//                                    }
//                                    finally {
//                                        startActivity(intent);
//                                        finish();
//                                    }
//                                }
//                            };
//                            timer.start();


                            break;

                        case 400:

//                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

//                            Toast.makeText(SplashActivity.this, "Failed", Toast.LENGTH_SHORT).show();



                            break;

                    }
//                progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.i("Hello", "onResponse: " + t.getMessage());
//                    Toast.makeText(SplashActivity.this, "Call Failed", Toast.LENGTH_SHORT).show();
                    final Intent intent = new Intent(SplashActivity.this, NoConnectionActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                }

            });

        }

        else{
            final Intent i = new Intent(this,MainActivity.class);
            Thread timer = new Thread(){
                public void run(){
                    try{
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally {
                        startActivity(i);
                        finish();
                    }
                }
            };
            timer.start();

        }









    }
    @Override
    protected void onResume() {
        //start handler as activity become visible

        handler.postDelayed( runnable = new Runnable() {
            public void run() {


                handler.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

// If onPause() is not included the threads will double up when you
// reload the activity

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }


}
