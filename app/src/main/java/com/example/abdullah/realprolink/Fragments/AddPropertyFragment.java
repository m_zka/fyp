package com.example.abdullah.realprolink.Fragments;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Activity.ProfileActivity;
import com.example.abdullah.realprolink.Model.CitySearchModel;
import com.example.abdullah.realprolink.Model.Property.AddPropertyResponse;
import com.example.abdullah.realprolink.Model.UploadResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.hbb20.CountryCodePicker;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.paperdb.Paper;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import ir.mirrajabi.searchdialog.core.Searchable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.INPUT_METHOD_SERVICE;


public class AddPropertyFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private TextView citytv;
    private TextView changeCity;
double latitude,longitude;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private List<AutocompletePrediction> predictionList;

    private FusedLocationProviderClient mFusedLocationProviderClientCity;
    private PlacesClient placesClientCity;
    private List<AutocompletePrediction> predictionListCity;




    private Location mLastKnownLocation;
    private LocationCallback locationCallback;

    private MaterialSearchBar materialSearchBar,materialSearchBar1;
    private View mapView;
    private Button btnFind;


    private final float DEFAULT_ZOOM = 15;




    private Button homebutton, plotbutton, commercialbutton;
    private Button salebutton, rentbutton;
    private ImageButton uploads;
    ClipData clipData;
    Api api = RetrofitClient.getApiClient().create(Api.class);
int type=0,purpose=0;
    String beds;
    String baths,code;

    EditText title,location,area,desc,price,p_email,p_phone;
    EditText min_bid;
    AppCompatTextView city;
    Button btnAdd;
    public AddPropertyFragment() {
        // Required empty public constructor
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {

             clipData=data.getClipData();
            if(clipData!=null){

               Paper.book().write("clipdata",clipData);
                }
            }
            //the image URI
//            Uri selectedImage = data.getData();
//
//            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image",Paper.book().read("user_id").toString());
        }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getActivity().getPackageName()));
            getActivity().finish();
            startActivity(intent);
            return;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity)getActivity()).setActionBarTitle("Add Property");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_property,container,false);
        //declare buttons
        materialSearchBar = view.findViewById(R.id.searchBar);
        materialSearchBar1=view.findViewById(R.id.searchBarCity);
        homebutton = view.findViewById(R.id.homeButton);
        plotbutton = view.findViewById(R.id.plotButton);
        commercialbutton = view.findViewById(R.id.commercialButton);
        rentbutton = view.findViewById(R.id.rentButton);
        salebutton = view.findViewById(R.id.saleButton);
        uploads=view.findViewById(R.id.btn_imagesUpload);



        //Edit Text

        title=view.findViewById(R.id.et_addPropery_title);
        desc=view.findViewById(R.id.et_addPropery_desc);
        price=view.findViewById(R.id.et_addPropery_price);
        area=view.findViewById(R.id.et_addPropery_area);
        p_email=view.findViewById(R.id.et_addPropery_contactEmail);
        p_phone=view.findViewById(R.id.editText_carrierNumber);

        min_bid=view.findViewById(R.id.et_addPropery_minBid);

        //Button

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        Places.initialize(getActivity(),"AIzaSyCAw94EuY9EnZMukKaec5R5qnS_D4CFpAM");
        placesClient = Places.createClient(getActivity());
        placesClientCity=Places.createClient(getActivity());
        final AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();


        materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                getActivity().startSearch(text.toString(), true, null, true);
            }

            @Override
            public void onButtonClicked(int buttonCode) {
                if (buttonCode == MaterialSearchBar.BUTTON_NAVIGATION) {
                    //opening or closing a navigation drawer
                } else if (buttonCode == MaterialSearchBar.BUTTON_BACK) {
                    materialSearchBar.disableSearch();
                }
            }
        });

        materialSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                FindAutocompletePredictionsRequest predictionsRequest = FindAutocompletePredictionsRequest.builder()
                        .setCountry("pk")
                        .setTypeFilter(TypeFilter.ADDRESS)
                        .setSessionToken(token)
                        .setQuery(s.toString())
                        .build();
                placesClient.findAutocompletePredictions(predictionsRequest).addOnCompleteListener(new OnCompleteListener<FindAutocompletePredictionsResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<FindAutocompletePredictionsResponse> task) {
                        if (task.isSuccessful()) {
                            FindAutocompletePredictionsResponse predictionsResponse = task.getResult();
                            if (predictionsResponse != null) {
                                predictionList = predictionsResponse.getAutocompletePredictions();
                                List<String> suggestionsList = new ArrayList<>();
                                for (int i = 0; i < predictionList.size(); i++) {
                                    AutocompletePrediction prediction = predictionList.get(i);
                                    suggestionsList.add(prediction.getFullText(null).toString());
                                }
                                materialSearchBar.updateLastSuggestions(suggestionsList);
                                if (!materialSearchBar.isSuggestionsVisible()) {
                                    materialSearchBar.showSuggestionsList();
                                }
                            }
                        } else {
                            Log.i("mytag", "prediction fetching task unsuccessful");
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        materialSearchBar.setSuggstionsClickListener(new SuggestionsAdapter.OnItemViewClickListener() {
            @Override
            public void OnItemClickListener(int position, View v) {
                if (position >= predictionList.size()) {
                    return;
                }

                AutocompletePrediction selectedPrediction = predictionList.get(position);
                String suggestion = materialSearchBar.getLastSuggestions().get(position).toString();
                materialSearchBar.setText(suggestion);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        materialSearchBar.clearSuggestions();
                    }
                }, 1000);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(materialSearchBar.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
                final String placeId = selectedPrediction.getPlaceId();
                final List<Place.Field> placeFields = Arrays.asList(Place.Field.LAT_LNG);

                FetchPlaceRequest fetchPlaceRequest = FetchPlaceRequest.builder(placeId, placeFields).build();
                placesClient.fetchPlace(fetchPlaceRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                    @Override
                    public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                        Place place = fetchPlaceResponse.getPlace();



                        Log.i("mytag", "Place found: " + place.getName());
                        LatLng latLngOfPlace = place.getLatLng();
                       // Toast.makeText(getContext(),place.getAddress(),Toast.LENGTH_LONG).show();


                        latitude=latLngOfPlace.latitude;
                       longitude=latLngOfPlace.longitude;
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            apiException.printStackTrace();
                            int statusCode = apiException.getStatusCode();
                            Log.i("mytag", "place not found: " + e.getMessage());
                            Log.i("mytag", "status code: " + statusCode);
                        }
                    }
                });
            }

            @Override
            public void OnItemDeleteListener(int position, View v) {

            }
        });





        materialSearchBar1.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                getActivity().startSearch(text.toString(), true, null, true);
            }

            @Override
            public void onButtonClicked(int buttonCode) {
                if (buttonCode == MaterialSearchBar.BUTTON_NAVIGATION) {
                    //opening or closing a navigation drawer
                } else if (buttonCode == MaterialSearchBar.BUTTON_BACK) {
                    materialSearchBar.disableSearch();
                }
            }
        });

        materialSearchBar1.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                FindAutocompletePredictionsRequest predictionsRequest = FindAutocompletePredictionsRequest.builder()
                        .setTypeFilter(TypeFilter.CITIES)
                        .setCountry("pk")
                        .setSessionToken(token)
                        .setQuery(s.toString())
                        .build();
                placesClientCity.findAutocompletePredictions(predictionsRequest).addOnCompleteListener(new OnCompleteListener<FindAutocompletePredictionsResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<FindAutocompletePredictionsResponse> task) {
                        if (task.isSuccessful()) {
                            FindAutocompletePredictionsResponse predictionsResponse = task.getResult();
                            if (predictionsResponse != null) {
                                predictionList = predictionsResponse.getAutocompletePredictions();
                                List<String> suggestionsList = new ArrayList<>();
                                for (int i = 0; i < predictionList.size(); i++) {
                                    AutocompletePrediction prediction = predictionList.get(i);
                                    suggestionsList.add(prediction.getFullText(null).toString());
                                }
                                materialSearchBar1.updateLastSuggestions(suggestionsList);
                                if (!materialSearchBar1.isSuggestionsVisible()) {
                                    materialSearchBar1.showSuggestionsList();
                                }
                            }
                        } else {
                            Log.i("mytag", "prediction fetching task unsuccessful");
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        materialSearchBar1.setSuggstionsClickListener(new SuggestionsAdapter.OnItemViewClickListener() {
            @Override
            public void OnItemClickListener(int position, View v) {
                if (position >= predictionList.size()) {
                    return;
                }
                AutocompletePrediction selectedPrediction = predictionList.get(position);
                String suggestion = materialSearchBar1.getLastSuggestions().get(position).toString();
                materialSearchBar1.setText(suggestion);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        materialSearchBar1.clearSuggestions();
                    }
                }, 1000);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(materialSearchBar1.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
                final String placeId = selectedPrediction.getPlaceId();
                final List<Place.Field> placeFields = Arrays.asList(Place.Field.LAT_LNG);

                FetchPlaceRequest fetchPlaceRequest = FetchPlaceRequest.builder(placeId, placeFields).build();
                placesClientCity.fetchPlace(fetchPlaceRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                    @Override
                    public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                        Place place = fetchPlaceResponse.getPlace();



                        Log.i("mytag", "Place found: " + place.getName());
                        LatLng latLngOfPlace = place.getLatLng();
                        // Toast.makeText(getContext(),place.getAddress(),Toast.LENGTH_LONG).show();


                        latitude=latLngOfPlace.latitude;
                        longitude=latLngOfPlace.longitude;
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            apiException.printStackTrace();
                            int statusCode = apiException.getStatusCode();
                            Log.i("mytag", "place not found: " + e.getMessage());
                            Log.i("mytag", "status code: " + statusCode);
                        }
                    }
                });
            }

            @Override
            public void OnItemDeleteListener(int position, View v) {

            }
        });

        btnAdd= view.findViewById(R.id.et_addPropery_btn);

        //spinner coding
        Spinner spinnerbedroom =view.findViewById(R.id.bedroom_spinner);
        Spinner spinnerbathroom =view.findViewById(R.id.bathroom_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),R.array.numbers,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerbedroom.setAdapter(adapter);
        spinnerbedroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                beds = parent.getItemAtPosition(position).toString();
                Toast.makeText(getActivity(),beds,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        spinnerbathroom.setAdapter(adapter);
        spinnerbathroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                baths = parent.getItemAtPosition(position).toString();
                Toast.makeText(getActivity(),beds,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });







        //spiiner coding end


        //country code

        CountryCodePicker ccp;
        EditText editTextCarrierNumber;

        ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        editTextCarrierNumber = (EditText) view.findViewById(R.id.editText_carrierNumber);
        ccp.registerCarrierNumberEditText(editTextCarrierNumber);
        ccp.isValidFullNumber();
//        ccp.setNumberAutoFormattingEnabled(false);
      code =  ccp.getDefaultCountryCodeWithPlus();

        ccp.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                // your code
            }
        });

        //country code end

        // buttons coding

        homebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                homebutton.setTextColor(Color.WHITE);
                type=0;
                plotbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                plotbutton.setTextColor(Color.BLUE);
                commercialbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                commercialbutton.setTextColor(Color.BLUE);
            }
        });

        plotbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plotbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                plotbutton.setTextColor(Color.WHITE);
                type=1;
                homebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                homebutton.setTextColor(Color.BLUE);
                commercialbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                commercialbutton.setTextColor(Color.BLUE);
            }
        });

        commercialbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commercialbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                commercialbutton.setTextColor(Color.WHITE);
                type=2;
                homebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                homebutton.setTextColor(Color.BLUE);
                plotbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                plotbutton.setTextColor(Color.BLUE);
            }
        });

        rentbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rentbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                rentbutton.setTextColor(Color.WHITE);
                purpose=0;
                salebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                salebutton.setTextColor(Color.BLUE);
            }
        });

        salebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salebutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.after_change_button));
                salebutton.setTextColor(Color.WHITE);
                purpose=1;
                rentbutton.setBackgroundDrawable(getResources().getDrawable(R.drawable.before_change_button));
                rentbutton.setTextColor(Color.BLUE);
            }
        });

        uploads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                i.setType("image/*");
                startActivityForResult(i, 1);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                uploadimages();


            }
        });





        //button coding end

        return view;
    }

    private ArrayList<CitySearchModel> intitData(){
        ArrayList<CitySearchModel> items = new ArrayList<>();
        items.add(new CitySearchModel("Lahore"));
        items.add(new CitySearchModel("Karachi"));
        items.add(new CitySearchModel("Faislabad"));
        items.add(new CitySearchModel("Multan"));
        items.add(new CitySearchModel("hello"));
        items.add(new CitySearchModel("Captian America"));

        return items;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void uploadimages() {



        if(clipData!=null){

            for(int i=0;i<clipData.getItemCount();i++){

                Uri imagUri=clipData.getItemAt(i).getUri();
                Toast.makeText(getActivity(),imagUri.toString(),Toast.LENGTH_SHORT).show();

                File file = new File(getRealPathFromURI(getActivity().getApplicationContext(),imagUri));
                MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

                RequestBody userBody = RequestBody.create(MediaType.parse("text/plain"),  title.getText().toString());


                Call<UploadResponse> call = api.uploadMultipleImage(filePart,userBody);

                //finally performing the call
                call.enqueue(new Callback<UploadResponse>() {
                    @Override
                    public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
                        //Toast.makeText(getContext(), "File Uploaded Successfully...", Toast.LENGTH_LONG).show();


//                        Intent i = new Intent(getActivity(), ProfileActivity.class);
//                        startActivity(i);
//                if (response.body().getMessage()=="File Uploaded Successfullly") {
//                } else {
//                    Toast.makeText(getContext(), "Some error occurred...", Toast.LENGTH_LONG).show();
//                }
                    }

                    @Override
                    public void onFailure(Call<UploadResponse> call, Throwable t) {
                       // Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });

            }
            addProperty();
        }

        else{
//            Toast.makeText(getContext(), "Select more than 1 image", Toast.LENGTH_LONG).show();

        }
        //the image URI
//            Uri selectedImage = data.getData();
//
//            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image",Paper.book().read("user_id").toString());

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String getRealPathFromURI(Context context, Uri contentURI) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(contentURI);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = { MediaStore.Images.Media.DATA };

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{ id }, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    private void addProperty() {


        Api api = RetrofitClient.getApiClient().create(Api.class);
        Call<AddPropertyResponse>call=  api.Addproperty(
                Paper.book().read("user_id").toString(),
                title.getText().toString(),"lahore","lahore",area.getText().toString(),
                 beds,baths,
                desc.getText().toString(),0,
                price.getText().toString(),
               type,purpose,
                p_email.getText().toString(),
                code.concat(p_phone.getText().toString()),
                longitude,latitude,min_bid.getText().toString());



        call.enqueue(new Callback<AddPropertyResponse>() {
            @Override
            public void onResponse(Call<AddPropertyResponse> call, Response<AddPropertyResponse> response) {

                int code = response.code();

                switch (code) {
                    case 200:
//                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.fMain,new HomeFragment());
                        ft.addToBackStack(null);
                        ft.commit();

                        break;

                    case 400:
//                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<AddPropertyResponse> call, Throwable t) {

            }
        });

    }






    //build in functions for spinners
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    //build in functions for spinners end
}
