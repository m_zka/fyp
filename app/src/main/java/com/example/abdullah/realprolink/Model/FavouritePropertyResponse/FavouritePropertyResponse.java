
package com.example.abdullah.realprolink.Model.FavouritePropertyResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavouritePropertyResponse {

    @SerializedName("data")
    @Expose
    private List<FavoruiteResponse> data = null;

    public List<FavoruiteResponse> getData() {
        return data;
    }

    public void setData(List<FavoruiteResponse> data) {
        this.data = data;
    }

}
