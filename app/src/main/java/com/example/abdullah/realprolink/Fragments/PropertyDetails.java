package com.example.abdullah.realprolink.Fragments;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdullah.realprolink.Activity.MainActivity;
import com.example.abdullah.realprolink.Activity.MapsActivity;
import com.example.abdullah.realprolink.Adapters.ViewPagerAdapter;
import com.example.abdullah.realprolink.Model.URLResponse.URLResponse;
import com.example.abdullah.realprolink.Model.URLResponse.URLS;
import com.example.abdullah.realprolink.Model.UserInfoResponse;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.Retrofit.Api;
import com.example.abdullah.realprolink.Retrofit.RetrofitClient;


import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PropertyDetails extends Fragment {
    Api api = RetrofitClient.getApiClient().create(Api.class);
List<URLS> URLS;
    List<UserInfoResponse> infoResponses;
    LinearLayout acceptBid;
    CardView cardView;
    LinearLayout bidDesc,bidderdDetail;
    Button cancelBTN;
LinearLayout maps;

    public PropertyDetails() {
        // Required empty public constructor
    }

TextView price,title,address,beds,p_id,purpose,location,baths,area,type,desc,current_bid,bid_name,bid_phone,bid_amount;
//    MyPropertyFragment myPropertyFragment= new MyPropertyFragment();
//   List<UserProperty> response=  myPropertyFragment.getPropertyResponse();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity)getActivity()).setActionBarTitle("Property Detail");
final View view = inflater.inflate(R.layout.fragment_property_details, container, false);

bidderdDetail=view.findViewById(R.id.biderdetail);
cancelBTN=view.findViewById(R.id.cancelBid);
final Call<URLResponse> call =api.getUri(Paper.book().read("d_title").toString());
call.enqueue(new Callback<URLResponse>() {
    @Override
    public void onResponse(Call<URLResponse> call, Response<URLResponse> response) {

URLS=response.body().getData();
int i= URLS.size();
        String[] strings=new String[i];
        for (int j=0;j<=i-1;j++){
            strings[j]="http://192.168.1.100:8080/FYP/api/uploads/".concat(URLS.get(j).getUrl());
//            Toast.makeText(getContext(),strings[j],Toast.LENGTH_LONG).show();

        }
//      murls.add(i)="http://onlinenoticeboard.website/api/uploads/".concat(String.valueOf(urls.get(i).getUrl()));
//
//}
        TabLayout mTabLayout = view.findViewById(R.id.tab_layout);
        ViewPager viewPager = view.findViewById(R.id.ViewPager_myproperties);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getContext(), strings);
        viewPager.setAdapter(viewPagerAdapter);
        mTabLayout.setupWithViewPager(viewPager, true);

    }



    @Override
    public void onFailure(Call<URLResponse> call, Throwable t) {

    }
});


price=view.findViewById(R.id.propertyDetail_price);
title=view.findViewById(R.id.propertyDetail_title);
        address=view.findViewById(R.id.propertyDetail_address);
        beds=view.findViewById(R.id.propertyDetail_beds);
        baths=view.findViewById(R.id.propertyDetail_baths);
        p_id=view.findViewById(R.id.propertyDetail_id);
        purpose=view.findViewById(R.id.propertyDetail_purpose);
        location=view.findViewById(R.id.propertyDetail_location);
        area=view.findViewById(R.id.propertyDetail_area);
        type=view.findViewById(R.id.propertyDetail_type);
desc=view.findViewById(R.id.propertyDetail_desc);
current_bid=view.findViewById(R.id.myProp_current_bid);

acceptBid=view.findViewById(R.id.myProp_Accept_bid);
bidDesc=view.findViewById(R.id.biddescription);
cardView=view.findViewById(R.id.bidderDetail);

bid_name=view.findViewById(R.id.propertyDetail_username);
bid_phone=view.findViewById(R.id.propertyDetail_userPhone);
bid_amount=view.findViewById(R.id.propertyDetail_bidammount);

maps=view.findViewById(R.id.myprop_maps);


        price.setText(Paper.book().read("d_price").toString());
        title.setText(Paper.book().read("d_title").toString());
        address.setText(Paper.book().read("d_address").toString());
        location.setText(Paper.book().read("d_address").toString());
        beds.setText(Paper.book().read("d_beds").toString());
        baths.setText(Paper.book().read("d_baths").toString());
        p_id.setText(Paper.book().read("d_pid").toString());

//
//


//
        area.setText(Paper.book().read("d_area").toString());
        desc.setText(Paper.book().read("d_desc").toString());



        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MapsActivity.class));
            }
        });



        switch (Paper.book().read("d_type").toString()){

            case "0":
                type.setText("House");
                break;
            case "1":
                type.setText("Plot");
                break;
            case "2":
                type.setText("Commercial");
                break;




        }

        switch (Paper.book().read("d_purpose").toString()){

            case "0":
                purpose.setText("Sale");
                break;
            case "1":
                purpose.setText("Rent");
                break;




        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                current_bid.setText(Paper.book().read("maxbid").toString());
                Api api = RetrofitClient.getApiClient().create(Api.class);

    if(Paper.book().read("biduserid")!=null){
    Call<UserInfoResponse> call = api.ReadUser(
            Paper.book().read("biduserid").toString()
    );

    call.enqueue(new Callback<UserInfoResponse>() {
        @Override
        public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {

            bid_name.setText(response.body().getFirstName().concat(" ").concat(response.body().getLastName()));
            bid_phone.setText(response.body().getUserMobile());
            bid_amount.setText(Paper.book().read("maxbid").toString());



        }

        @Override
        public void onFailure(Call<UserInfoResponse> call, Throwable t) {

        }
    });
}


                else{
                   bid_name.setText(" ");
                   bid_phone.setText(" ");
                   bid_amount.setText(" ");
                   acceptBid.setVisibility(View.INVISIBLE);

               }




            }
        },50);



        if(Integer.parseInt(Paper.book().read("d_status").toString()) == 1){
            bidDesc.setVisibility(View.GONE);
            bidderdDetail.setVisibility(View.VISIBLE);



        }

        else{


            acceptBid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Toast.makeText(getContext(),Paper.book().read("acceptbid".concat("accept")).toString(),Toast.LENGTH_LONG).show();
if(Integer.parseInt(Paper.book().read("check").toString()) == 0){

    if(Integer.parseInt(Paper.book().read("d_status").toString()) == 0) {


        final Call<ResponseBody> call = api.acceptBid(Paper.book().read("biduserid").toString(), p_id.getText().toString());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(getContext(),"Bid Accepted",Toast.LENGTH_LONG).show();
                Paper.book().write("d_status",1);
                FragmentTransaction ft = getFragmentManager().beginTransaction();

                ft.detach(PropertyDetails.this);
                ft.attach(PropertyDetails.this);
                ft.disallowAddToBackStack();

                ft.commitAllowingStateLoss();



            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });





    }

    else {

        Toast.makeText(getContext(),"Bid Already Accepted",Toast.LENGTH_LONG).show();
    }




}
else {

    Toast.makeText(getContext(),"Cant Bid on Own Property",Toast.LENGTH_LONG).show();


}

                }
            });
        }




        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                Call<ResponseBody> call = api.cancelBid(
                        Paper.book().read("biduserid").toString(),Paper.book().read("d_pid").toString()
                );
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Paper.book().write("d_status",0);
                        FragmentTransaction ft = getFragmentManager().beginTransaction();

                        ft.detach(PropertyDetails.this);
                        ft.attach(PropertyDetails.this);

                        ft.addToBackStack(null);
                        ft.commit();


                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });




            }
        });


        return  view;

    }

}
