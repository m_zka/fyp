package com.example.abdullah.realprolink.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.abdullah.realprolink.Fragments.AgentsListFragement;
import com.example.abdullah.realprolink.Fragments.BidedPropertiesFragement;
import com.example.abdullah.realprolink.Fragments.AddPropertyFragment;
import com.example.abdullah.realprolink.Fragments.CommissionFragment;
import com.example.abdullah.realprolink.Fragments.CompareFragment;
import com.example.abdullah.realprolink.Fragments.FavouriteFragment;
import com.example.abdullah.realprolink.Fragments.HomeFragment;
import com.example.abdullah.realprolink.Fragments.LoginFragment;
import com.example.abdullah.realprolink.Fragments.MortgageFragment;
import com.example.abdullah.realprolink.Fragments.MyPropertyFragment;

import com.example.abdullah.realprolink.Fragments.UserProfileFragment;
import com.example.abdullah.realprolink.Fragments.working.SearchFragment;
import com.example.abdullah.realprolink.Fragments.PropertiesFragement;
import com.example.abdullah.realprolink.R;
import com.example.abdullah.realprolink.nearbyFragement;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
ImageButton imgBTN;
    Button btnHome,btnPlot,btn_Comm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    Paper.init(this);


        setContentView(R.layout.activity_main2);


      //  sendEmail();



//ProfileApi();






        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Paper.init(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);



        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();




        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(Paper.book().read("Email")!=null){

            if(Integer.parseInt(Paper.book().read("user_type").toString())==1 || Integer.parseInt(Paper.book().read("user_type").toString())==2){
                navigationView.getMenu().setGroupVisible(R.id.login_features,true);


            }
//            navigationView.getMenu().setGroupVisible(R.id.login_features,true);

            ImageButton ib=navigationView.getHeaderView(0).findViewById(R.id.nav_login_btn);
            ib.setVisibility(View.INVISIBLE);
            TextView tx =navigationView.getHeaderView(0).findViewById(R.id.nav_name);
            tx.setVisibility(View.VISIBLE);
          //  tx.setText(Paper.book().read("lname").toString());
           tx.setText(Paper.book().read("Email").toString());
        }



        // default fragment
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fMain,new HomeFragment());
        ft.commit();

        navigationView.setCheckedItem(R.id.home);
    }



    public void toLogin(View v){
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        ft.replace(R.id.fragment_container,new LoginFragment());
//        ft.addToBackStack(null);
//        ft.commit();
        Intent i = new Intent(MainActivity.this, Login.class);
        startActivity(i);
    }

    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }  else if (getFragmentManager().getBackStackEntryCount() > 0 ){
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new HomeFragment());
            ft.addToBackStack(null);
            ft.commit();

        } else if (id == R.id.nearbyhomes) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new nearbyFragement());
            ft.addToBackStack(null);
            ft.commit();
        }

        else if (id == R.id.allProperties) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new PropertiesFragement());
            ft.addToBackStack(null);
            ft.commit();

        }

        else if (id == R.id.search_property) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new SearchFragment());
            ft.addToBackStack(null);
            ft.commit();
        } else if (id == R.id.compareproperty) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new CompareFragment());
            ft.addToBackStack(null);
            ft.commit();
        } else if (id == R.id.mortgage_calc) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new MortgageFragment());
            ft.addToBackStack(null);
            ft.commit();

        } else if (id == R.id.commissio_calc) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new CommissionFragment());
            ft.addToBackStack(null);
            ft.commit();
        }else if (id == R.id.favourite) {
            if(Paper.book().read("Email")!=null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new FavouriteFragment());
            ft.addToBackStack(null);
            ft.commit();}
            else {

                startActivity(new Intent(this,Login.class));
                finish();
            }
        }else if (id == R.id.add_property) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new AddPropertyFragment());
            ft.addToBackStack(null);
            ft.commit();
        }
        else if (id == R.id.my_property) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new MyPropertyFragment());
            ft.addToBackStack(null);
            ft.commit();
        }
        else if (id == R.id.bided_properties) {

            if(Paper.book().read("Email")!=null){
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fMain,new BidedPropertiesFragement());
                ft.addToBackStack(null);
                ft.commit();
            }
            else {

                startActivity(new Intent(this,Login.class));
                finish();

            }

        }
        else if (id == R.id.agent_list) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new AgentsListFragement());

            ft.addToBackStack(null);
            ft.commit();

        }


        else if (id == R.id.bided_property) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fMain,new BidedPropertiesFragement());
            ft.addToBackStack(null);
            ft.commit();

        }
        else if(id==R.id.logout){
            if(Paper.book().read("Email")!=null){

            Paper.book().delete("Email");
            Paper.book().delete("Password");
            Paper.book().delete("user_id");


            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent);
            finish();}
else {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fMain,new LoginFragment());
                ft.addToBackStack(null);
                ft.commit();
            }
        }





        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void profile(View view) {

        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }






//    }
}
