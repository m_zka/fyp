
package com.example.abdullah.realprolink.Model.AgentListResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentsListResponse {

    @SerializedName("data")
    @Expose
    private List<AgentsResponse> data = null;

    public List<AgentsResponse> getData() {
        return data;
    }

    public void setData(List<AgentsResponse> data) {
        this.data = data;
    }

}
