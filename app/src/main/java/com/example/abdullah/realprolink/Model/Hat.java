package com.example.abdullah.realprolink.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Hat implements Parcelable {


    private int image;

    public Hat(int image) {
        this.image = image;

    }

    public Hat(Hat product) {
        this.image = product.image;

    }


    protected Hat(Parcel in) {
        image = in.readInt();
    }

    public static final Creator<Hat> CREATOR = new Creator<Hat>() {
        @Override
        public Hat createFromParcel(Parcel in) {
            return new Hat(in);
        }

        @Override
        public Hat[] newArray(int size) {
            return new Hat[size];
        }
    };


    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(image);
    }
}