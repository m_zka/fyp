package com.example.abdullah.realprolink.Model;

import ir.mirrajabi.searchdialog.core.Searchable;

public class CitySearchModel implements Searchable {

    private String mTitle;


    public CitySearchModel(String mTitle){
        this.mTitle = mTitle;
    }

    public void setmTitle(String mTitle){
        this.mTitle=mTitle;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }
}
