
package com.example.abdullah.realprolink.Model.URLResponse;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class URLResponse {

    @SerializedName("data")
    @Expose
    private List<URLS> data = null;

    public List<URLS> getData() {
        return  data;
    }

    public void setData(List<URLS> data) {
        this.data = data;
    }

}
